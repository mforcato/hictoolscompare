
setwd("./simulations_interactions")

library(tools)

noise_Values<-c("56_0.01_.TIME","56_0.01_addIntConstant","84_0.01_.TIME","84_0.01_addIntConstant","112_0.01_.TIME","112_0.01_addIntConstant","140_0.01_.TIME","140_0.01_addIntConstant")

all_metrics_table<-NULL

methods<-c("homer","diffhic","fithic","hiccups")
trueTADs_median<-NULL
foundTADs_median<-NULL

for (noise in noise_Values) {
  
  trueTADsfile_list<-dir("trueLoops/")[grep(noise,dir("trueLoops/"))]
  
  if(length(trueTADsfile_list)!=5) print(paste(noise,"warning"))
  
  for (trueTAD_i in trueTADsfile_list) {

    trueTADsfile<-file.path("trueLoops",trueTAD_i)
    trueTADs<-read.table(trueTADsfile, sep="\t", header=T, stringsAsFactors=F)
    trueTADsList<-paste(trueTADs$anchor, trueTADs$target,sep="_")

    
    distance_trueTADs<-mean(trueTADs$anchor - trueTADs$target)
    trueTADs_median<-c(trueTADs_median,distance_trueTADs)
    
    simul_ID<-strsplit(file_path_sans_ext(trueTAD_i),split="-")[[1]][3]

    for (method in methods) {

      print(paste(method, noise))
      foundTADsfile<-dir(file.path("foundLoops",method))[grep(simul_ID, dir(file.path("foundLoops",method)))]
      if(length(foundTADsfile)!=1) print("warning")
     
      if (method=="homer") {
      
       foundTADs<-read.table(file.path("foundLoops",method,foundTADsfile,"significantInteractions_bins.txt"), sep="\t", header=T, stringsAsFactors=F) #ok tutti anchor>target
       colnames(foundTADs)<-c("anchor","target")
      
      }  else if (method=="hiccups" ) {
      
         foundTADs<-read.table(file.path("foundLoops",method,foundTADsfile,"merged_loops"), sep="\t", header=T, stringsAsFactors=F)
         foundTADs$target<-foundTADs$x2/40000
         foundTADs$anchor<-foundTADs$y2/40000
         sum(foundTADs$anchor > foundTADs$target)
         foundTADs<- foundTADs[,c("anchor","target")] 

      }  else  foundTADs<-read.table(file.path("foundLoops",method,foundTADsfile), sep="\t", header=T, stringsAsFactors=F)
     
      if (method=="diffhic") { 
         foundTADs<- foundTADs[,c("start_1.x", "start_2.x")]
         colnames(foundTADs)<-c("anchor","target")    
      }
      if (method=="fithic" | method=="fithic_withDiagonal" ) { 
         foundTADs<- foundTADs[,c("targets", "anchors")] 
         colnames(foundTADs)<-c("anchor","target")    
      }
     
      
      foundTADsList<-paste(foundTADs$anchor,foundTADs$target,sep="_")
      distance_foundTADs<-mean(foundTADs$anchor - foundTADs$target)
      foundTADs_median<-c(foundTADs_median,distance_foundTADs)
      
      ### compare TADs
      TP_TADs<-sum(foundTADsList %in% trueTADsList)
      FP_TADs<-sum(!foundTADsList %in% trueTADsList)
      FN_TADs<-sum(!trueTADsList %in% foundTADsList)

      
      ###sensitivity or true positive rate (TPR)(Recall)
      TPR_TADs<-TP_TADs/(TP_TADs+FN_TADs)*100
      ### precision or positive predictive value (PPV)
      PPV_TADs <- TP_TADs/(TP_TADs+FP_TADs)*100 
      FDR_TADs<- 100 - PPV_TADs
      called_TADs<-length(foundTADsList)
      
      all_metrics<-c(noise, method, called_TADs, TP_TADs, FP_TADs, TPR_TADs, PPV_TADs, FDR_TADs) 
      all_metrics_table<-rbind(all_metrics_table,all_metrics) 
    
    } # end for on methods

  }# end for on trueTADsfile_list

} # end for on noise

colnames(all_metrics_table)<-c("simul_ID","method","called_TADs","TP_TADs","FP_TADs","TPR_TADs","PPV_TADs","FDR_TADs")
rownames(all_metrics_table)<-NULL


all_metrics_table<-cbind("noise"=all_metrics_table[,"simul_ID"],all_metrics_table)
all_metrics_table[,"simul_ID"]<-gsub("_0.01_.TIME","",all_metrics_table[,"simul_ID"])
all_metrics_table[,"simul_ID"]<-gsub("_0.01","",all_metrics_table[,"simul_ID"])

pippo<-as.matrix(all_metrics_table[,4:9])
mode(pippo)<-"numeric"
final_table<-data.frame(all_metrics_table[,1:3], pippo, stringsAsFactors=F)

final_table$simul_ID<-factor(final_table$simul_ID, levels=c("56","56_addIntConstant","84","84_addIntConstant","112","112_addIntConstant","140","140_addIntConstant"))

trueLoops_median<-data.frame(trueTADs_median*40,"noise"=rep(noise_Values,each=5), stringsAsFactors=F)

save.image("simulations.loops.results.RData")
write.table(final_table,"simulations.loops.results.txt",sep="\t", quote=F, row.names=F)

##################################################################################

load("simulations.loops.results.RData")

library(plyr)

cdata_TADs <- ddply(final_table, c("simul_ID", "method"), summarise, mean_TP = mean(TP_TADs), se_TP=sd(TP_TADs)/sqrt(5), mean_TPR = mean(TPR_TADs), se_TPR=sd(TPR_TADs)/sqrt(5), mean_PPV = mean(PPV_TADs), se_PPV=sd(PPV_TADs)/sqrt(5), mean_called= mean(called_TADs), se_called= sd(called_TADs)/sqrt(5), mean_FP = mean(FP_TADs), se_FP=sd(FP_TADs)/sqrt(5), mean_FDR = mean(FDR_TADs), se_FDR=sd(FDR_TADs)/sqrt(5))

cdata_TADs$simul_ID<-as.character(cdata_TADs$simul_ID)
cdata_TADs$new_method<-ifelse(grepl("addIntConstant",cdata_TADs$simul_ID),paste(cdata_TADs$method,"costant",sep="_"),cdata_TADs$method)
cdata_TADs$new_simul_ID<-gsub("_addIntConstant","",cdata_TADs$simul_ID)
cdata_TADs$new_simul_ID<-factor(cdata_TADs$new_simul_ID, levels=c("56","84","112","140"))

cdata_TADs_subset<- cdata_TADs[cdata_TADs$simul_ID %in% c("56","84","112","140"),]
cdata_TADs_subset$method<-factor(cdata_TADs_subset$method, levels=c("hiccups","homer","diffhic","fithic"))

### plot

library(ggplot2)
library(scales) 
cols<-c("#a50026","#FDAE61","#FEE090","#4575B4","#a50026","#FDAE61","#FEE090","#4575B4")
simul_type<-ifelse(grepl("addIntConstant",cdata_TADs$simul_ID),"2","1")

pd <- position_dodge(0)
plots<-list()

plots[["called_nocostant"]]<-ggplot(cdata_TADs_subset, aes(x=new_simul_ID, y=mean_called, colour=method, group=new_method)) +
    geom_errorbar(aes(ymin=mean_called-se_called, ymax=mean_called+se_called), width=.1, position=pd) +
    geom_line(size=1, position=pd)  +  scale_colour_manual(values=cols) +
    geom_point(size=1,position=pd) + theme(plot.background = element_blank(),panel.background = element_rect(fill = "white",color = "black"),panel.grid.major = element_blank(),panel.grid.minor =  element_blank(), axis.text.x = element_text(size=8, angle = 90, hjust = 1), legend.position="none")  + ggtitle("Loops nocostant")  + ylab("called (x1000)") + geom_hline(yintercept = 1000,linetype = 2) + scale_y_log10(breaks=c(1000,10000,100000),labels=c(1,10,100)) 

cdata_TADs$new_method<-factor(cdata_TADs$new_method, levels=c("hiccups","homer","diffhic","fithic","hiccups_costant","homer_costant","diffhic_costant","fithic_costant"))

plots[["TPR"]]<-ggplot(cdata_TADs, aes(x=new_simul_ID, y=mean_TPR, colour=new_method, group=new_method)) +
    geom_errorbar(aes(ymin=mean_TPR-se_TPR, ymax=mean_TPR+se_TPR), width=.1, position=pd) +
    geom_line(aes(linetype=simul_type), size=1, position=pd)  +  scale_colour_manual(values=cols) +
    geom_point(size=1,position=pd) + theme(plot.background = element_blank(),panel.background = element_rect(fill = "white",color = "black"),panel.grid.major = element_blank(),panel.grid.minor =  element_blank(), axis.text.x = element_text(size=8, angle = 90, hjust = 1),legend.position="none")  + ggtitle("Loops") + scale_y_continuous(limits=c(0,100))  + ylab("TPR")

plots[["FDR"]]<-ggplot(cdata_TADs, aes(x=new_simul_ID, y=mean_FDR, colour=new_method, group=new_method)) +
    geom_errorbar(aes(ymin=mean_FDR-se_FDR, ymax=mean_FDR+se_FDR), width=.1, position=pd) +
    geom_line(aes(linetype=simul_type),size=1, position=pd)  +  scale_colour_manual(values=cols) +
    geom_point(size=1,position=pd) + theme(plot.background = element_blank(),panel.background = element_rect(fill = "white",color = "black"),panel.grid.major = element_blank(),panel.grid.minor =  element_blank(), axis.text.x = element_text(size=8, angle = 90, hjust = 1),legend.position="none")  + ggtitle("Loops") + scale_y_continuous(limits=c(0,100))  + ylab("FDR")

library(gridExtra)
ml <- marrangeGrob(plots, nrow=1, ncol=1, top=NULL)
ggsave("SuppFigure8A_8D_8E.pdf", ml, width=7, height=7)


##################################################################################
library("RColorBrewer")
cols<-c("#a50026","#FDAE61","#FEE090","#4575B4")
load("simulations.loops.results.RData")

final_table<-cbind(final_table,foundTADs_median*40)
final_table$method<-factor(final_table$method, levels=c("hiccups","homer","diffhic","fithic"))
min_val<-min(final_table$foundTADs_median)
max_val<-  max(final_table$foundTADs_median)

pdf("SuppFigure8C.pdf")
for (noise in noise_Values) {

  true_size_.25<-quantile(trueLoops_median[trueLoops_median$noise==noise,1],.25)
  true_size_.75<-quantile(trueLoops_median[trueLoops_median$noise==noise,1],.75)

  sel_table<-final_table[final_table$noise==noise,]

  boxplot(sel_table$foundTADs_median ~ sel_table$method, log="y",col=cols, ylab="average distance (kb)", main=paste("noise",noise), las=2, ylim=c(min(min_val,true_size_.25), max_val))
  lines(abline(h=true_size_.25, lty=2))
  lines(abline(h=true_size_.75, lty=2))
  
  boxplot(sel_table$foundTADs_median ~ sel_table$method, log="y",col=cols, ylab="average distance (kb)", main=paste("noise",noise), las=2)
  lines(abline(h=true_size_.25, lty=2))
  lines(abline(h=true_size_.75, lty=2))
}
dev.off()


