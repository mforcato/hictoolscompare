
################################################################################

base_dir<-"./Hi-C_dataset"
dataset<-"Rao" 

cell_line<-"GM12878" 

setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
bin_size<-40000
source("./compare_tads/00_utils_tads.r")
enzyme<-"MboI"#"DpnII" #"MboI"

### convert to simple format and get descriptive statistics

### armatus
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  armatus_rep<-file.path(base_dir,dataset,"armatus",paste(dataset,rep,cell_line,enzyme,"armatus.txt",sep="_"))
  tads_summary_from_file(armatus_rep,working_dir)
}


### HiCseg
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  hicseg_rep<-file.path(base_dir, dataset, "HiCseg", paste(dataset,rep,cell_line,enzyme,"HiCseg.txt",sep="_"))
  tads_summary_from_file(hicseg_rep,working_dir)
}

### DomainCaller
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")

for (rep in replicates) {
  domaincaller_rep<-file.path(base_dir, dataset, "domainCaller", paste(dataset,rep,cell_line,enzyme,"domainCaller.txt",sep="_"))
  tads_summary_from_file(domaincaller_rep,working_dir)
}

#### tadbit
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  tadbit_rep<-file.path(base_dir, dataset, "TADBit", paste(dataset,rep,cell_line,enzyme,"TADBit.txt",sep="_"))
  tads_summary_from_file(tadbit_rep,working_dir)
}

### tadtree
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  tadtree_rep<-file.path(base_dir, dataset,"TADtree",paste(dataset,rep,cell_line,enzyme,"TADtree.txt",sep="_"))
  tads_summary_from_file(tadtree_rep,working_dir)
}

### Insulation Score
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  ins_rep<-file.path(base_dir, dataset,"InsulationScore",paste(dataset,rep,cell_line,enzyme,"InsulationScore.txt",sep="_"))
  tads_summary_from_file(ins_rep,working_dir)
}

### Arrowhead
if (enzyme=="MboI") replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH") else replicates<-c("RepA1","RepA2","RepA3","RepB1","RepB2")
for (rep in replicates) {
  arrowhead_rep<-file.path(base_dir,dataset,"Arrowhead",paste(dataset,rep,cell_line,enzyme,"Arrowhead.txt",sep="_"))
  tads_summary_from_file(arrowhead_rep,working_dir)
}

################################################################################
### remove enzyme name
cd ./compare_tads/Rao/GM12878/
rename 's/_MboI_/_/' *.txt
rename 's/_DpnII_/_/' *.txt

################################################################################
### compare replicates

base_dir<-"./Hi-C_dataset"
dataset<-"Rao" 

cell_line<-"GM12878" 

setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
bin_size<-40000
source("./compare_tads/00_utils_tads.r")

replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH","RepA1","RepA2","RepA3","RepB1","RepB2") 

for (method in c("TADBit","armatus","domainCaller","HiCseg","TADtree","InsulationScore","Arrowhead")) compare_tads_with_string_approach(dataset, cell_line, replicates, method, write_universe=T, quit_after_writing=F)


### Jaccard Index
replicates<-c("RepA","RepB","RepC1","RepC2","RepD","RepE1", "RepE2","RepF","RepG1", "RepG2", "RepH","RepA1","RepA2","RepA3","RepB1","RepB2") 
for (method in c("TADBit","armatus","domainCaller","HiCseg","TADtree","InsulationScore","Arrowhead"))  calculate_concordance(dataset, cell_line, replicates, method, threshold=2)

################################################################################
### compare with CTCF

dataset<-"Rao" 
cell_line<-"GM12878"

setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
source("./compare_tads/00_utils_tads.r")

for (method in c("TADBit","armatus","domainCaller","HiCseg","TADtree","InsulationScore","Arrowhead")) compare_with_genomic_track(dataset, cell_line, method, track="CTCF")



