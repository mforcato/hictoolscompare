
################################################################################
### plots for JACCARD INDEX
################################################################################

library(ggplot2)
library(reshape2)
library(scales)
library("RColorBrewer")

interazioni<-read.table("tads_concordance_all_pairwise_JI.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
library("RColorBrewer")

selected_subset_few_columns<-interazioni
selected_subset_few_columns$Dataset<-paste(selected_subset_few_columns$Dataset, selected_subset_few_columns$Cell_line)
selected_subset_few_columns$Dataset<-factor(selected_subset_few_columns$Dataset, levels=c("Lieberman-Aiden GM06990","Dixon2012 hESC","Dixon2012 IMR90","Jin IMR90","Rao GM12878","Rao IMR90_MboI","Dixon2015 H1_hESC"))

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")
plots<-list()
selected_subset_few_columns$Method<-factor(selected_subset_few_columns$Method, levels=c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore"))

################################################################################
### Supplementary Figure 9A
### all pairwise JI, for each dataset
################################################################################

plots[[1]]<-ggplot(selected_subset_few_columns, aes(x=Method, y=JI)) + geom_boxplot(fill=rep(cols,7)) + facet_grid(~Dataset) + ggtitle(paste("")) + labs(y="JI") + theme(plot.background = element_blank(),panel.background = element_rect(fill = "white",color = "black"),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),axis.title.x = element_blank(),axis.ticks.x=element_blank(), axis.text.x = element_text(size=8, angle = 90, hjust = 1),legend.position="none",strip.background = element_rect(color = "black"))  + scale_y_continuous(labels=function(n){format(n, scientific = F)}) 

library(gridExtra)

ml <- marrangeGrob(plots, nrow=1, ncol=1, top=NULL)
ggsave("SuppFigure9A.pdf", ml, width = 13)


################################################################################
### Figure 3D
### all pairwise JI, for all datasets
################################################################################

pdf("Figure3D_boxplot_JI_all_datasets.pdf",width=10)
par(mfrow=c(1,1))

boxplot(selected_subset_few_columns$JI ~ selected_subset_few_columns$Method, col=cols, main=paste("all datasets"),ylab="JI")

dev.off()

################################################################################
### plots for OVERLAP COEFFICIENT
################################################################################

interazioni<-read.table("tads_concordance_all_pairwise_OC.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)  #non copiare file dal server ma aggiungi a mano, in coda, i nuovi valori

selected_subset_few_columns<-interazioni
selected_subset_few_columns$Dataset<-paste(selected_subset_few_columns$Dataset, selected_subset_few_columns$Cell_line)
selected_subset_few_columns$Dataset<-factor(selected_subset_few_columns$Dataset, levels=c("Lieberman-Aiden GM06990","Dixon2012 hESC","Dixon2012 IMR90","Jin IMR90","Rao GM12878","Rao IMR90_MboI","Dixon2015 H1_hESC"))

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")
plots<-list()
selected_subset_few_columns$Method<-factor(selected_subset_few_columns$Method, levels=c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore"))

################################################################################
### Supplementary Figure 9C
### all pairwise OC, for all datasets
################################################################################

pdf("SuppFigure9C.pdf",width=10)
par(mfrow=c(1,1))
boxplot(selected_subset_few_columns$OC ~ selected_subset_few_columns$Method, col=cols, main=paste("all datasets"),ylab="OC")
dev.off()

