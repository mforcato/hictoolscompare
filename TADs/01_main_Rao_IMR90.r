
################################################################################

base_dir<-"./Hi-C_dataset"
dataset<-"Rao" 

cell_line<-"IMR90" 

setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
bin_size<-40000
source("./compare_tads/00_utils_tads.r")
enzyme<-"MboI"

### convert to simple format and get descriptive statistics

### armatus
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")   
for (rep in replicates) {
  armatus_rep<-file.path(base_dir,dataset,"armatus",paste(dataset,rep,cell_line,enzyme,"armatus.txt",sep="_"))
  tads_summary_from_file(armatus_rep,working_dir)
}

### HiCseg
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (rep in replicates) {
  hicseg_rep<-file.path(base_dir, dataset, "HiCseg", paste(dataset,rep,cell_line,enzyme,"HiCseg.txt",sep="_"))
  tads_summary_from_file(hicseg_rep,working_dir)
}

### DomainCaller
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (rep in replicates) {
  domaincaller_rep<-file.path(base_dir, dataset, "domainCaller", paste(dataset,rep,cell_line,enzyme,"domainCaller.txt",sep="_"))
  tads_summary_from_file(domaincaller_rep,working_dir)
}

#### tadbit
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")  
for (rep in replicates) {
  tadbit_rep<-file.path(base_dir, dataset, "TADBit", paste(dataset,rep,cell_line,enzyme,"TADBit.txt",sep="_"))
  tads_summary_from_file(tadbit_rep,working_dir)
}

### tadtree
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (rep in replicates) {
  tadtree_rep<-file.path(base_dir, dataset,"TADtree",paste(dataset,rep,cell_line,enzyme,"TADtree.txt",sep="_"))
  tads_summary_from_file(tadtree_rep,working_dir)
}

### Insulation Score
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (rep in replicates) {
  ins_rep<-file.path(base_dir, dataset,"InsulationScore",paste(dataset,rep,cell_line,enzyme,"InsulationScore.txt",sep="_"))
  tads_summary_from_file(ins_rep,working_dir)
}

### Arrowhead
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (rep in replicates) {
  arrowhead_rep<-file.path(base_dir,dataset,"Arrowhead",paste(dataset,rep,cell_line,enzyme,"Arrowhead.txt",sep="_"))
  tads_summary_from_file(arrowhead_rep,working_dir)
}

### compare replicates
################################################################################

base_dir<-"./Hi-C_dataset"
dataset<-"Rao" 

cell_line<-"IMR90" 

setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
bin_size<-40000
source("./compare_tads/00_utils_tads.r")
enzyme<-"MboI"

replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")

for (method in c("domainCaller","TADBit","armatus", "HiCseg","TADtree","InsulationScore","Arrowhead")) compare_tads_with_string_approach(dataset, paste(cell_line, enzyme,sep="_"),replicates,method, write_universe=T, quit_after_writing=F)

### Jaccard Index
replicates<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")
for (method in c("domainCaller","TADBit","armatus", "HiCseg","TADtree","InsulationScore","Arrowhead"))  calculate_concordance(dataset, paste(cell_line, enzyme,sep="_"), replicates, method, threshold=2)

################################################################################
### compare with CTCF

dataset<-"Rao" 
cell_line<-"IMR90"
enzyme<-"MboI" 
setwd(file.path("./compare_tads",dataset,cell_line))
working_dir<-getwd()
source("./compare_tads/00_utils_tads.r")

for (method in c("domainCaller","TADBit","armatus", "HiCseg","TADtree","InsulationScore","Arrowhead"))  compare_with_genomic_track(dataset, cell_line, method, track="CTCF")


