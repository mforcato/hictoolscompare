################################################################################
tads_summary_from_file<-function(filename, working_dir, outfilename="summary.txt") {
  file1<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 

  row.has.na <- apply(file1, 1, function(x){any(is.na(x))})
  file1 <- file1[!row.has.na,]

  num_filename1<-nrow(file1)
 
  mean_size<-as.integer(mean(file1$V3 - file1$V2))
  median_size<-as.integer(median(file1$V3 - file1$V2))
  
  boundaries_id<-unique(c(paste(file1$V1,file1$V2,sep="_"),paste(file1$V1,file1$V3,sep="_")))
  num_boundaries<-length(boundaries_id)  
  
  nomi<-strsplit(gsub(".txt","",basename(filename)),split="_")[[1]]
  final_result<-paste(c(num_filename1,mean_size,median_size,num_boundaries,nomi),collapse="\t")
  write.table(final_result, file.path(working_dir, outfilename),sep="\t",col.names=F, row.names=F, quote=F, append=T) 
  
  print(paste(basename(filename),num_filename1,mean_size,median_size))
  write.table(file1, file.path(working_dir,basename(filename)),sep="\t",quote=F,col.names=F,row.names=F) 

}

################################################################################
compare_tads_with_string_approach<-function(dataset, cell_line, replicates, method, threshold=2, write_universe=F, quit_after_writing=F, outfilename=NULL) {

  require(plyr)
  if (is.null(outfilename)) outfilename<-paste(dataset, cell_line, method, sep="_")
  boundaries_union<-NULL
  
  for (rep_i in replicates) {
  
    tads_i<-read.table(paste(dataset,"_",rep_i,"_",cell_line,"_",method,".txt",sep=""), header=F, sep='\t', stringsAsFactors=F, fill=T) 

    boundaries_id<-unique(c(paste(tads_i$V1,tads_i$V2,sep="_"),paste(tads_i$V1,tads_i$V3,sep="_")))
    boundaries_id<-cbind(boundaries_id, rep(rep_i, length(boundaries_id)))
                   
    boundaries_union<-rbind(boundaries_union, boundaries_id)
  } # end for
  
  boundaries_union<-as.data.frame(boundaries_union, stringsAsFactors=F)
  boundaries_union_intersection<-ddply(boundaries_union, .(boundaries_id), summarize, where = paste(V2, collapse=","), number=length(V2))
  
  if (write_universe) {
    write.table(boundaries_union_intersection, paste(outfilename, "universe.txt",sep="_"), sep="\t", quote=F, row.names=F)
  }
  
  if (quit_after_writing) return()    
  
  inatleast2<-boundaries_union_intersection[boundaries_union_intersection$number >= threshold,]  
  boundaries_inatleast2<-inatleast2$boundaries_id
  
  final_result<-paste(c(dataset,cell_line, method, length(boundaries_inatleast2)),collapse="\t")
  write.table(final_result, "./compare_tads/TAD_common_summary.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T) 

  boundaries_entry<-do.call(rbind,strsplit(boundaries_inatleast2,"_")) 
  formatted_boundaries<-data.frame(boundaries_entry[,1], as.integer(boundaries_entry[,2]), stringsAsFactors=F)
  write.table(formatted_boundaries, paste(outfilename,"boundaries_common.txt",sep="_"), quote=F, row.names=F, col.names=F, sep="\t")

}

################################################################################

split_tads_single_replicate<-function(dataset, cell_line, replicates, method)     {
  
  tads_i<-read.table(paste(dataset,"_",replicates,"_",cell_line,"_",method,".txt",sep=""), header=F, sep='\t', stringsAsFactors=F, fill=T) 

  boundaries_id<-unique(c(paste(tads_i$V1,tads_i$V2,sep="_"),paste(tads_i$V1,tads_i$V3,sep="_")))
  boundaries_entry<-do.call(rbind,strsplit(boundaries_id,"_")) 
  
  formatted_boundaries<-data.frame(boundaries_entry[,1], as.integer(boundaries_entry[,2]), stringsAsFactors=F)
  write.table(formatted_boundaries,paste(dataset,cell_line,method,"boundaries_common.txt",sep="_"), quote=F, row.names=F, col.names=F, sep="\t")

}

################################################################################

calculate_concordance<-function(dataset, cell_line, replicates, method, threshold=2) { 

  boundaries_interactions_intersection<-read.table(paste(dataset,cell_line, method, "universe.txt",sep="_"), sep="\t", stringsAsFactors=F, fill=T, header=T)
  boundaries_interactions_intersection$where<-paste(boundaries_interactions_intersection$where,",",sep="")
  
  replicates_ordered<-replicates
  replicates_ordered<-paste(replicates_ordered,",",sep="")
  
  JI_matrix<-matrix(rep(NA,length(replicates)^2),nrow=length(replicates))
  rownames(JI_matrix)<-replicates_ordered
  colnames(JI_matrix)<-replicates_ordered

  OC_matrix<-JI_matrix

  pippo<-boundaries_interactions_intersection$where
  all_comb<-combn(replicates_ordered,2)
  
  for (comb_i in 1:ncol(all_comb)) {

    i<-all_comb[1,comb_i]
    j<-all_comb[2,comb_i]
    
    jacc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/sum(grepl(i,pippo) | grepl(j,pippo))
    oc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/min(c(sum(grepl(i,pippo)),sum(grepl(j,pippo))))

    JI_matrix[i,j]<-jacc_ij
    OC_matrix[i,j]<-oc_ij

    }
 
  final_interactions<-sum(boundaries_interactions_intersection$number >= threshold)

  ### JI
  all_pairwise_JI<-JI_matrix[!is.na(JI_matrix)]

  JI_total<-final_interactions/nrow(boundaries_interactions_intersection)
  JI_average<-mean(all_pairwise_JI)
  JI_median<-median(all_pairwise_JI)

  final_result<-paste(c(dataset, cell_line, method, final_interactions, round(JI_total,5), round(JI_average,5), round(JI_median,5)),collapse="\t")
  write.table(final_result, "./compare_tads/concordance/TAD_concordance_summary_JI.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  write.table(cbind(dataset,cell_line,method,all_pairwise_JI), "./compare_tads/concordance/tads_concordance_all_pairwise_JI.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)

  ### OC
  all_pairwise_OC<-OC_matrix[!is.na(OC_matrix)]
  OC_total<-final_interactions/nrow(boundaries_interactions_intersection)
  OC_average<-mean(all_pairwise_OC)
  OC_median<-median(all_pairwise_OC)
  final_result<-paste(c(dataset, cell_line, method, final_interactions, round(OC_total,5), round(OC_average,5), round(OC_median,5)),collapse="\t")
  write.table(final_result, "./compare_tads/concordance/peaks_concordance_summary_OC.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  write.table(cbind(dataset,cell_line,method,all_pairwise_OC), "./compare_tads/concordance/peaks_concordance_all_pairwise_OC.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)

}

################################################################################
plot_heatmap_with_legend<-function(JI_matrix, key.xlab="score", axes.labels, main.title, heatmap.colors, type="enzyme"){

  scale01 <- function(x, low = min(x), high = max(x)) {
      x <- (x - low)/(high - low)
      x
  }
  lhei <- c(1, 4)
  lwid <- c(1.5, 4)
  lmat <- rbind(4:3, 2:1)
  lmat <- rbind(lmat[1, ] + 1, c(NA, 1), lmat[2, ] + 1)
  lhei <- c(lhei[1], 0.2, lhei[2])
  lmat[is.na(lmat)] <- 0
  margins = c(5, 5)
  layout(lmat, widths = lwid, heights = lhei, respect = FALSE)

  if (type=="enzyme") ColSideColors<- c(rep("darkgreen",5), rep("darkmagenta",11)) else ColSideColors<- c(rep("darkgreen",7), rep("darkmagenta",6))
  par(mar = c(0.5, 0, 0, margins[2]))
  image(cbind(1:ncol(JI_matrix)), col = ColSideColors, axes = FALSE)

  par(mar = c(5, 0, 2, 5))
  if (type=="enzyme") {
  image(JI_matrix, col=heatmap.colors,  main="DpnII                                         MboI", axes=F, zlim=c(0,1))  } else {
  image(JI_matrix, col=heatmap.colors,  main="  Rao                                       Jin", axes=F, zlim=c(0,1)) }
  grey_matrix<-matrix(rep(NA,length(axes.labels)^2),nrow=length(axes.labels)) 
  diag(grey_matrix)<-1
  image(grey_matrix, col="grey",  add=T, axes=F)
  axis(1,at=seq(0,1,length.out=length(axes.labels)),labels=axes.labels,cex.axis=0.8,las=2)
  axis(2,at=seq(0,1,length.out=length(axes.labels)),labels=axes.labels,cex.axis=0.8,las=2)
   
  plot.new()
  plot.new()   
  title(method, cex.main = 1.5)
  mar <- c(5, 4, 2, 1)
  mar[1] <- 2
  par(mar = mar, cex = 0.75, mgp = c(2, 1, 0))
  
  col<-heatmap.colors
  #min.raw <- min(JI_matrix, na.rm = TRUE)
  min.raw <- 0
  #max.raw <- max(JI_matrix, na.rm = TRUE)
  max.raw <- 1
  breaks <- length(col) + 1
  breaks <- seq(min.raw, max.raw, length = breaks)
  tmpbreaks <- breaks
  z <- seq(min.raw, max.raw, length = length(col))
  image(z = matrix(z, ncol = 1), col = col, breaks = tmpbreaks, xaxt = "n", yaxt = "n")
  
  par(usr = c(0, 1, 0, 1))
  lv <- pretty(breaks)
  xv <- scale01(as.numeric(lv), min.raw, max.raw)
  xargs <- list(at = xv, labels = lv, cex.axis=0.8)
  xargs$side <- 1
  do.call(axis, xargs)
  key.xlab <- key.xlab
  mtext(side = 1, key.xlab, line = par("mgp")[1], padj = 0.5, cex=0.7)
}

################################################################################
compare_with_genomic_track<-function(dataset, cell_line, method, track="CTCF", max.distance=20000, calculate.profile=T) {

  library(ChIPpeakAnno)
  
  ### load CTCF
  if (cell_line=="hESC" | cell_line=="H1_hESC") regionpeakfile2<-"./compare_tads/tracks/CTCF/peaks/wgEncodeAwgTfbsBroadH1hescCtcfUniPk.narrowPeak"
  if (cell_line=="IMR90") regionpeakfile2<-"./compare_tads/tracks/CTCF/peaks/wgEncodeAwgTfbsSydhImr90CtcfbIggrabUniPk.narrowPeak"
  if (cell_line=="GM12878") regionpeakfile2<-"./compare_tads/tracks/CTCF/peaks/wgEncodeAwgTfbsBroadGm12878CtcfUniPk.narrowPeak"
  if (cell_line=="GM06990") regionpeakfile2<-"./compare_tads/tracks/CTCF/peaks/wgEncodeAwgTfbsUwGm06990CtcfUniPk.narrowPeak"
  if (cell_line=="FlyEmbryos" & track == "CTCF") regionpeakfile2<-"./compare_tads/tracks/CTCF/peaks/5069_repset.17400882.enrichment.clusters.narrowPeak"
  if (cell_line=="FlyEmbryos" & track == "BEAF32") regionpeakfile2<-"./compare_tads/tracks/BEAF32/peaks/3954_repset.17400048.enrichment.clusters.narrowPeak"
  
  encode_peak_j<-read.table(regionpeakfile2, header=F, sep='\t', stringsAsFactors=F)
  encode_peak_j$V4<-paste("peak",1:nrow(encode_peak_j),sep="_")
  summit_j<-encode_peak_j$V2 +  encode_peak_j$V10
  rangedData2 <- BED2RangedData(data.frame(encode_peak_j$V1, summit_j, summit_j, encode_peak_j$V4, stringsAsFactors=F))
  
  ### select tad calling method

  regionpeakfile1<-file.path("./compare_tads",dataset,cell_line,paste(dataset,cell_line,method,"boundaries_common.txt",sep="_"))
  if (dataset=="Rao" & cell_line=="IMR90") regionpeakfile1<-file.path("./compare_tads",dataset,cell_line,paste(dataset,paste(cell_line,"MboI",sep="_"),method,"boundaries_common.txt",sep="_")) 
  boundaries_i<-read.table(regionpeakfile1, header=F, sep='\t', stringsAsFactors=F)
  boundaries_i$V3<-as.integer(boundaries_i$V2+1)
  boundaries_i$V4<-paste(boundaries_i$V1,boundaries_i$V2,sep="_")
  
  rangedData1 <- BED2RangedData(data.frame(boundaries_i$V1, boundaries_i$V2, boundaries_i$V2, boundaries_i$V4, stringsAsFactors=F))
  
  ### search nearest CTCF site
  annotatedPeak <- annotatePeakInBatch(rangedData1, AnnotationData=rangedData2)
  final_peaks <-as.data.frame(annotatedPeak)
  distances<-final_peaks$distancetoFeature
  
  # percentage of boundaries with a peak within max.distance
  max.distance<-as.integer(max.distance)
  num_boundaries<-nrow(final_peaks)
  boundary_with_ctcf_num<-sum(abs(distances) < max.distance)
  boundary_with_ctcf_percent<-sum(abs(distances) < max.distance)/num_boundaries*100

  final_result<-paste(dataset,cell_line,method,num_boundaries,boundary_with_ctcf_num,boundary_with_ctcf_percent,collapse="\t")
  write.table(final_result, paste("./compare_tads/TAD_boundaries_vs",track,"summary.txt",sep="_"),sep="\t",col.names=F, row.names=F, quote=F, append=T) 

  if (calculate.profile) {
    ### search for all CTCF sites in a 1MB window around boundary
    
    offset<-500000
    rangedData1_window <- BED2RangedData(data.frame(boundaries_i$V1, boundaries_i$V2-offset, boundaries_i$V2+offset, boundaries_i$V4, stringsAsFactors=F))
    annotatedPeak_all_sites <- annotatePeakInBatch(rangedData1_window, AnnotationData=rangedData2, PeakLocForDistance = "middle", output="overlapping", select="all")
    final_peaks_all_sites <-as.data.frame(annotatedPeak_all_sites)
    distances_all_sites<-final_peaks_all_sites$distancetoFeature
    
    pluto<-hist(distances_all_sites, breaks=seq(-500000,+500000, length.out=101), plot=F)
    valori<-pluto$counts/num_boundaries
    valori_final<-c(dataset,cell_line,method,valori)
    write.table(paste(valori_final,collapse="\t"), paste("./compare_tads/TAD_boundaries_vs",track,"values_for_plot_summary.txt",sep="_"),sep="\t",col.names=F, row.names=F, quote=F, append=T) 
  }
} # end function