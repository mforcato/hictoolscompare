### convert gff3 to narrowpeak format

setwd("./compare_tads/tracks/CTCF/peaks")  
filename<-"5069_repset.17400882.enrichment.clusters.gff3"

file1<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 
file1$V1<-paste("chr",file1$V1,sep="")

file_narrowpeak<-file1
file_narrowpeak$V2<-file1$V4
file_narrowpeak$V3<-file1$V5
file_narrowpeak$V4<-file1$V9
file_narrowpeak$V5<-file1$V7
file_narrowpeak$V6<-file1$V7
file_narrowpeak$V9<-file1$V7
file_narrowpeak$V10<-round((file_narrowpeak$V3-file_narrowpeak$V2)/2)

write.table(file_narrowpeak,"5069_repset.17400882.enrichment.clusters.narrowPeak",quote=F, sep="\t",row.names=F,col.names=F)

################################################################################

setwd("./compare_tads/tracks/BEAF32/peaks")  
filename<-"3954_repset.17400048.enrichment.clusters.gff3"

file1<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 

file1$V1<-paste("chr",file1$V1,sep="")

file_narrowpeak<-file1
file_narrowpeak$V2<-file1$V4
file_narrowpeak$V3<-file1$V5
file_narrowpeak$V4<-file1$V9
file_narrowpeak$V5<-file1$V7
file_narrowpeak$V6<-file1$V7
file_narrowpeak$V9<-file1$V7
file_narrowpeak$V10<-round((file_narrowpeak$V3-file_narrowpeak$V2)/2)

write.table(file_narrowpeak,"3954_repset.17400048.enrichment.clusters.narrowPeak",quote=F, sep="\t",row.names=F,col.names=F)
