
################################################################################
### CTCF enrichment around TAD boundaries
################################################################################

set<-read.table("TAD_boundaries_vs_CTCF_values_for_plot_summary.txt",sep="\t",header=F,stringsAsFactors=F)
set$V1<-paste(set$V1,set$V2,sep="_")

all_datasets<-c("Sexton_FlyEmbryos","Dixon2012_hESC","Dixon2012_IMR90","Jin_IMR90","Rao_GM12878","Rao_IMR90","Dixon2015_H1_hESC")
cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")

tools<-sort(unique(set$V3))
tools<-c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore")

names(cols)<-tools

pdf("SuppFigure12a.pdf",width=14)
par(mfrow=c(3,7))
for (dataset in all_datasets) {
  
  for (tool in tools) {

    selection<-set$V1== dataset & set$V3 == tool
    peaks<-as.vector(unlist(set[selection,4:103]))
    plot(x=seq(-500,490,by=10),y=peaks,type="l",lwd=2,col=cols[tool],ylab="",xlab="Distance from TAD boundaries (Kb)",xaxt="n",main=paste(dataset, tool),las=1)
    axis(side=1,at=seq(-500,500,250))
    }
}

dev.off()

################################################################################
### BEAF32 enrichment around TAD boundaries
################################################################################

set<-read.table("TAD_boundaries_vs_BEAF32_values_for_plot_summary.txt",sep="\t",header=F,stringsAsFactors=F)
set$V1<-paste(set$V1,set$V2,sep="_")

all_datasets<-c("Sexton_FlyEmbryos")
cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")
tools<-sort(unique(set$V3))
tools<-c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore")

names(cols)<-tools

pdf("SuppFigure12b.pdf",width=14)
par(mfrow=c(3,7))
for (dataset in all_datasets) {
  for (tool in tools) {
    selection<-set$V1== dataset & set$V3 == tool
    peaks<-as.vector(unlist(set[selection,4:103]))
    plot(x=seq(-500,490,by=10),y=peaks,type="l",lwd=2,col=cols[tool],ylab="",xlab="Distance from TAD boundaries (Kb)",xaxt="n",main=paste(dataset, tool),las=1)
    axis(side=1,at=seq(-500,500,250))
    }
}

dev.off()
