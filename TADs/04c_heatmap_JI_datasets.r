###############################################################################
### heatmaps for inter-dataset JACCARD INDEX
################################################################################

library(RColorBrewer)

basedir<-"./compare_tads"
cell_line<-"IMR90"
bin_size<-40000
setwd(file.path(basedir))
source("./compare_tads/00_utils_tads.r")

dataset1<-"Rao" 
samplename1<-"MboI"
replicates1<-c("RepA1","RepA2","RepA3","RepA4","RepA5","RepB1","RepB2")

dataset2<-"Jin" 
replicates2<-c("RepA","RepB","RepC","RepD","RepE","RepF")

methods<-c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore")

### read statistics to order replicates
stats<-read.table("TADs_HiCpipe_postFiltering_statistics.txt", sep="\t", stringsAsFactors=F, header=T, fill=T)
stats1<-stats[stats$Dataset==dataset1 & stats$Cell_line==cell_line,]
replicates1_ordered<-stats1$Replicate[order(stats1$Post.filtering,decreasing=T)]
stats2<-stats[stats$Dataset==dataset2 & stats$Cell_line==cell_line,]
replicates2_ordered<-stats2$Replicate[order(stats2$Post.filtering,decreasing=T)]

for (method in methods) 
{
  cis_interactions_intersection1<-read.table(file.path(basedir,dataset1,cell_line,paste(dataset1,cell_line, samplename1, method, "universe.txt",sep="_")), sep="\t", stringsAsFactors=F, fill=T, header=T)
  cis_interactions_intersection2<-read.table(file.path(basedir,dataset2,cell_line,paste(dataset2,cell_line, method, "universe.txt",sep="_")), sep="\t", stringsAsFactors=F, fill=T, header=T)
  library(plyr)
  all_interactions<-rbind(cis_interactions_intersection1,cis_interactions_intersection2)
  all_interactions_unique<-ddply(all_interactions, .(boundaries_id), summarize, where = paste(where, collapse=","))
  
  all_interactions_unique$where<-paste(all_interactions_unique$where,",",sep="")
  
  ### order replicates in terms of decreasing number of interactions
  replicates_ordered<-c(replicates1_ordered,replicates2_ordered)
  replicates_ordered<-paste(replicates_ordered,",",sep="")

  JI_matrix<-matrix(rep(NA,length(replicates_ordered)^2),nrow=length(replicates_ordered))
  rownames(JI_matrix)<-replicates_ordered
  colnames(JI_matrix)<-replicates_ordered

  OC_matrix<-JI_matrix

  pippo<-all_interactions_unique$where
  all_comb<-combn(replicates_ordered,2)
  
  for (comb_i in 1:ncol(all_comb)) {

    i<-all_comb[1,comb_i]
    j<-all_comb[2,comb_i]
    
    jacc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/sum(grepl(i,pippo) | grepl(j,pippo))
    oc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/min(c(sum(grepl(i,pippo)),sum(grepl(j,pippo))))

    JI_matrix[i,j]<-jacc_ij
    OC_matrix[i,j]<-oc_ij

    }

  assign(paste("JI_matrix",method,sep="_"), JI_matrix)
  assign(paste("OC_matrix",method,sep="_"), OC_matrix)

} #end for methods 


replicates<-replicates_ordered

### calculate inter-dataset JI

for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  inter_JI_matrix<-JI_matrix[paste(replicates1_ordered,",",sep=""),paste(replicates2_ordered,",",sep="")]
  
  all_pairwise_JI<-inter_JI_matrix[!is.na(inter_JI_matrix)]
  inter_JI_average<-mean(all_pairwise_JI)
  inter_JI_median<-median(all_pairwise_JI) 
  
  final_result<-paste(method,inter_JI_average,inter_JI_median,collapse="\t")
  write.table(final_result, paste("Inter-dataset",cell_line,dataset1,"vs",dataset2,"concordance_TADs.txt",sep="_"), sep="\t", quote=F, row.names=F, col.names=F, append=T)

  write.table(cbind(method,all_pairwise_JI), paste("Inter-dataset",cell_line,dataset1,"vs",dataset2,"concordance_TADs_all_pairwise_JI.txt",sep="_"),sep="\t",col.names=F, row.names=F, quote=F, append=T)

}

################################################################################
### Supplementary Figure 11A 
### Inter-dataset JI heatmaps
################################################################################

replicates<-gsub(",","",replicates_ordered)

pdf("SuppFigure11A.pdf")
for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  plot_heatmap_with_legend(JI_matrix, "Jaccard Index", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Reds", n=9))(100), type="datasets")
}
for (method in methods) {
  OC_matrix<-get(paste("OC_matrix",method,sep="_"))
  plot_heatmap_with_legend(OC_matrix, "Overlap Coefficient", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Blues", n=9))(100), type="datasets")
}
dev.off()

################################################################################
### Supplementary Figure 11B 
### inter-dataset JI boxplot
################################################################################
cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")

interazioni<-read.table("Inter-dataset_IMR90_Rao_vs_Jin_concordance_TADs_all_pairwise_JI.txt",sep="\t",stringsAsFactors=F, header=F,fill=T)
colnames(interazioni)<-c("Method","value")
interazioni$Method<-factor(interazioni$Method, levels=c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore"))
mxy <- interazioni
 
pdf("SuppFigure11B.pdf",width=10)
boxplot(mxy$value ~ mxy$Method, col=cols, main="IMR90 Jin vs Rao",ylab="JI")
dev.off()
