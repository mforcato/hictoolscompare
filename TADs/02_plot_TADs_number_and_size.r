
interazioni<-read.table("TAD_summary.txt", sep="\t", stringsAsFactors=F, header=T, fill=T)

stats<-read.table("TAD_postFiltering_statistics.txt", sep="\t", stringsAsFactors=F, header=T, fill=T)

interazioni$id<-paste(interazioni$Dataset,interazioni$Cell_line,interazioni$Replicate,interazioni$Enzyme,sep="_")
stats$id<-paste(stats$Dataset,stats$Cell_line,stats$Replicate,stats$Enzyme,sep="_")
sum(stats$id %in% interazioni$id)

rownames(stats)<-stats$id
interazioni$Post.filtering<-stats[interazioni$id, "Post.filtering"]

write.table(interazioni, "TAD_summary_with_stats.txt", sep="\t", quote=F, row.names=F)

################################################################################
### Figure 3A
### scatterplot number of TADs vs number of reads
################################################################################

library("RColorBrewer")

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")

column_for_x_axis<-"Post.filtering"
column_for_y_axis<-"TADs.num" 

### filters

selection<-1:nrow(interazioni)
selection<-interazioni$Dataset!="Lieberman-Aiden"

### apply filters
selected_subset<-interazioni[selection,]
selected_subset<-selected_subset[!is.na(selected_subset$TADs.num),]

selected_subset$Method<-factor(selected_subset$Method, levels=c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore"))
method_list<-levels(selected_subset$Method)

### plot 
pdf("Figure3A.pdf")

plot(selected_subset[,column_for_x_axis], selected_subset[,column_for_y_axis], col=cols[as.factor(selected_subset$Method)], pch=20, xlab="#reads after filtering", ylab="#TADs", main="all datasets 40kb")

### add lines

for (i in 1:length(method_list)) {
  method<-method_list[i]
  y<-selected_subset[selected_subset$Method==method,column_for_y_axis]
  x<-selected_subset[selected_subset$Method==method,column_for_x_axis]
  logm1 <- lm( y ~ x)
  abline(logm1, col=cols[i], lwd=3)

}

dev.off()

################################################################################
### Figure 3B
### boxplot of TAD sizes
################################################################################

selection<-interazioni$Dataset!="Lieberman-Aiden"

### apply filters
selected_subset<-interazioni[selection,]

library("RColorBrewer")
cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4","#313695")

selected_subset$Method<-factor(selected_subset$Method, levels=c("Arrowhead","armatus","domainCaller","HiCseg","TADBit","TADtree", "InsulationScore"))

pdf("Figure3B.pdf")
boxplot(selected_subset$TADs.size.median~selected_subset$Method,col=cols, ylab="TADs median size per sample", main="all dataset 40kb", las=2)
boxplot(selected_subset$TADs.size.median~selected_subset$Method, log="y",col=cols, ylab="TADs median size per sample (log)", main="all dataset 40kb", las=2)

boxplot(selected_subset$TADs.size.mean~selected_subset$Method,col=cols, ylab="TADs mean size per sample", main="all dataset 40kb", las=2)
boxplot(selected_subset$TADs.size.mean~selected_subset$Method, log="y",col=cols, ylab="TADs mean size per sample (log)", main="all dataset 40kb", las=2)

dev.off()

