
################################################################################
### interactions vs TP and TN evidences 
################################################################################

TP_all<-read.table("peaks_vs_TP_new_formatted.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
TP_all$found.noadj_percent<-TP_all$found_noadj_num/TP_all$TP_noadj_num*100

TP_or_TN<-"TN" # "TP"

if (TP_or_TN == "TP") TP_common<-TP_all[TP_all$Replicate == "common" & TP_all$TP_type=="TP",] else TP_common<-TP_all[TP_all$Replicate == "common" & TP_all$TP_type=="TN",]

TP_common$ID<-paste(TP_common$Source_technique,TP_common$Source,TP_common$Dataset,TP_common$Cell_line)

### recover number of interactions called
interazioni<-read.table("all_datasets_comparison_with_chromatin_states.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
interazioni_cis<-interazioni[interazioni$interaction.type=="cis",]
rownames(interazioni_cis)<- paste(interazioni_cis$Dataset,interazioni_cis$Cell_line, interazioni_cis$Method)                                        
sum(paste(TP_common$Dataset,TP_common$Cell_line, TP_common$Method) %in% rownames(interazioni_cis) ) 
TP_common$number_called<- interazioni_cis[ paste(TP_common$Dataset,TP_common$Cell_line, TP_common$Method),  "num_interactions"]

TP_common$Method<-factor(TP_common$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))

if (TP_or_TN == "TP") {
   order_TP<-rev(c("3C Hou Sexton FlyEmbryo","3C Sexton Sexton FlyEmbryo","3C Jin Dixon_2012 IMR90","3C Jin Jin IMR90","3C Jin Rao IMR90","3C He Rao GM12878","5C Ferraiuolo Dixon_2012 H1_hESC","5C Ferraiuolo Dixon_2015 H1_hESC","5C Ferraiuolo Jin H1_hESC","5C Ferraiuolo Dixon_2012 IMR90", "5C Ferraiuolo Jin IMR90", "5C Ferraiuolo Rao IMR90","5C Ferraiuolo Rao GM12878", "5C Sanyal Dixon_2012 H1_hESC","5C Sanyal Dixon_2015 H1_hESC", "5C Sanyal Jin H1_hESC", "5C Sanyal Rao GM12878",  "ChIA-PET Ji Dixon_2012 H1_hESC", "ChIA-PET Ji Dixon_2015 H1_hESC", "ChIA-PET Ji Jin H1_hESC" ,"FISH3D Rao Rao GM12878"))
TP_common$ID<-factor(TP_common$ID, levels=order_TP) } else {
  order_TN<-rev(c("3C Kim Dixon_2012 H1_hESC","3C Kim Dixon_2015 H1_hESC","3C Kim Jin H1_hESC", "3C Kim Dixon_2012 IMR90", "3C Kim Jin IMR90", "3C Kim Rao IMR90","3C Kim Rao GM12878", "3C He Rao GM12878" ,   "5C Smith Rao GM12878" ,"ChIA-PET Ji Dixon_2012 H1_hESC", "ChIA-PET Ji Dixon_2015 H1_hESC",  "ChIA-PET Ji Jin H1_hESC" ,"FISH3D Rao Rao GM12878" ) )
  TP_common$ID<-factor(TP_common$ID, levels=order_TN)   
}

################################################################################
### Fig. 2F and SuppFigure 7D
### Performances in the identification of TP/TN evidences of cis interactions
################################################################################

library(gridExtra)
library(ggplot2)

TP_common$overlap<- TP_common$found.noadj_percent

TP_plot<-ggplot(TP_common, aes(x = Method, y = ID, size = overlap)) +  geom_point()  +  aes_string(color=TP_common$number_called) +  scale_colour_gradient(low="blue", high="red", name="number of interactions")

if (TP_or_TN == "TP") ggsave("Figure2F.pdf",TP_plot, width = 6.5)  else  ggsave("SuppFigure7D.pdf",TP_plot, width = 6.5) 

################################################################################
### SuppFigure 7C
### %TP from 5C data of Sanyal et al. recalled in Rao GM12878 vs reads number
################################################################################

interazioni<-read.table("all_datasets_summary.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
TP_all<-read.table("peaks_vs_TP_new_formatted.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)

TP<-TP_all[TP_all$Replicate !="common" & TP_all$TP_TN_list=="Sanyal_5C_TP",]

dataset<-"Rao"
cell_line<-"GM12878"

interazioni<-interazioni[interazioni$Dataset == dataset & interazioni$Cell_line == cell_line,]
TP<-TP[TP$Dataset == dataset & TP$Cell_line == cell_line,]

interazioni$Replicate_enz<-paste(interazioni$Enzyme,interazioni$Replicate,sep="_")

id_TP<-paste(TP$Dataset, TP$Cell_line, TP$Replicate, TP$Method)
id_interazioni<-paste(interazioni$Dataset, interazioni$Cell_line, interazioni$Replicate_enz, interazioni$Method)
sum(id_TP %in% id_interazioni)

rownames(interazioni)<-id_interazioni
TP$num_cis<-interazioni[id_TP, "cis"]

library("RColorBrewer")
cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")

TP$found_percent<-TP$found_num/TP$TP_num*100
TP$found.noadj_percent<-TP$found_noadj_num/TP$TP_noadj_num*100

### plot

column_for_x_axis<-"num_cis"
column_for_y_axis<-"found.noadj_percent" 

### filters
selected_subset<-TP
selected_subset$Method<-factor(selected_subset$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
method_list<-c("hiccups","gothic","homer","diffhic","hippie","fithic")

pdf("SuppFigure7C.pdf")

#### plot log x  + lowess
plot(selected_subset[,column_for_x_axis], selected_subset[,column_for_y_axis], col=cols[as.factor(selected_subset$Method)], pch=16, xlab="#cis interactions", ylab="%TP found", main=paste(dataset, cell_line), log="x")
for (i in 1:length(method_list)) {
  method<-method_list[i]
  lines(lowess(selected_subset[selected_subset$Method==method,column_for_y_axis] ~ selected_subset[selected_subset$Method==method,column_for_x_axis]),col=cols[i], lwd=3)
}

dev.off()
