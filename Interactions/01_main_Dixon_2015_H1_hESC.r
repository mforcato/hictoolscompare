
base_dir<-"./Hi-C_dataset"
cell_line<-"H1_hESC"
dataset<-"Dixon_2015" 
samplename<-"Hs_Normal_HindIII"
bin_size<-5000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

### convert to simple format and get descriptive statistics

### homer
replicates<-c("rA_merge","rB_merge")
for (rep in replicates) {

  homer_rep<-file.path(base_dir,dataset,"homer",paste(cell_line,samplename,rep,"homer.txt",sep="_"))
  format_homer(homer_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"homer_formatted.txt",sep="_"))

}

### fithic
replicates<-c("rA_merge","rB_merge")
for (rep in replicates) {

  fithic_rep<-file.path(base_dir,dataset,"fithic",paste(cell_line,samplename,rep,"fithic.txt",sep="_"))
  format_fithic(fithic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"fithic_formatted.txt",sep="_"))

}

### diffhic
replicates<-c("rA_merge","rB_merge")
for (rep in replicates) {
              
  diffhic_rep<-file.path(base_dir,dataset,"diffhic",paste(cell_line,samplename,rep,"diffhic.txt",sep="_"))
  format_diffhic_as_bin(diffhic_rep, paste(cell_line,samplename,rep,sep="_"),bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"diffhic_formatted.txt",sep="_"))

}

### hippie
replicates<-c("rA_merge","rB_merge")
for (rep in replicates) {
              
  hippie_rep<-file.path(base_dir,dataset,"hippie",paste(cell_line,samplename,rep,"hippie.txt"),sep="_")
  format_hippie_as_bin(hippie_rep, paste(cell_line,samplename,rep,sep="_"),bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"hippie_formatted.txt",sep="_"))

}

### hiccups
replicates<-c("rA_merge","rB_merge")                                     
for (rep in replicates) {
              
  hiccups_rep<-file.path(base_dir,dataset,"hiccups",paste(cell_line,samplename,rep,"hiccups.txt"),sep="_")
  format_hiccups(hiccups_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"hiccups_formatted.txt",sep="_"))

}
################################################################################
###  compare replicates

### compare replicates
base_dir<-"./Hi-C_dataset"
cell_line<-"H1_hESC"
dataset<-"Dixon_2015" 
samplename<-"Hs_Normal_HindIII"
bin_size<-5000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

replicates<-c("rA_merge","rB_merge")
methods<-c("homer","diffhic","hippie","fithic","hiccups")
for (method in methods) compare_interactions_with_string_approach(cell_line, samplename, replicates, bin_size, method, write_universe=T, quit_after_writing=F)

### Jaccard Index
replicates<-c("rA_merge","rB_merge")
methods<-c("homer","diffhic","hippie","fithic","hiccups")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="cis", threshold=2)
methods<-c("homer","diffhic","hippie")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="trans", threshold=2)


################################################################################
### compare common interactions with chromatin states

dataset<-"Dixon_2015" 
cell_line<-"H1_hESC"
samplename<-"Hs_Normal_HindIII"


setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

methods<-c("homer","diffhic","hippie")
for (method in methods) {
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="cis")
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="trans")
}
compare_with_chromatin_states(dataset, cell_line, samplename, "fithic", interactions_type="cis")
compare_with_chromatin_states(dataset, cell_line, samplename, "hiccups", interactions_type="cis")

################################################################################
### compare with TP
source("./compare_peaks/00_utils_interactions.r")

working_dir<-"./compare_peaks"
setwd(working_dir)

dataset<-"Dixon_2015"
cell_line<-"H1_hESC" 
samplename<-"Hs_Normal_HindIII"
bin_size<-5000

methods<-c("homer","diffhic","hippie","fithic","hiccups")
replicates<-c("rA_merge","rB_merge")

TP_list<-file.path("./compare_peaks/TP", paste("H1_hESC_5C_significant_formatted_",bin_size,".txt",sep=""))
TP_name<-"Sanyal_5C"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Ji_ChIA-PET_H1-hESC_TP_formatted_",bin_size,".txt",sep=""))
TP_name<-"Ji_ChIA-PET_TP"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Ji_ChIA-PET_H1-hESC_TN_formatted_",bin_size,".txt",sep=""))
TP_name<-"Ji_ChIA-PET_TN"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Ferraiuolo_5C_allCellLines_TP_formatted_",bin_size,".txt",sep=""))
TP_name<-"Ferraiuolo_5C_TP"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Kim_3C_nonErythroid_TN_formatted_",bin_size,".txt",sep=""))
TP_name<-"Kim_3C_TN"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}


