###############################################################################
### heatmaps for inter-enzyme JACCARD INDEX
################################################################################

base_dir<-"./Hi-C_dataset"
dataset<-"Rao" 
cell_line<-"GM12878"
samplename<-"Hs_InSitu"
bin_size<-5000

working_dir<-getwd()
setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

stats<-read.table("./compare_peaks/all_datasets_summary.txt", sep="\t", stringsAsFactors=F, header=T, fill=T)
stats1<-stats[stats$Dataset==dataset & stats$Cell_line==cell_line & stats$Enzyme=="DpnII" & stats$Method=="homer",]
replicates1_ordered<-stats1$Replicate[order(stats1$Total_reads,decreasing=T)]
stats2<-stats[stats$Dataset==dataset & stats$Cell_line==cell_line & stats$Enzyme=="MboI"& stats$Method=="homer",]
replicates2_ordered<-stats2$Replicate[order(stats2$Total_reads,decreasing=T)]

replicates<-c(replicates1_ordered,replicates2_ordered)

require(plyr)

methods<-c("hiccups","homer","gothic","diffhic","hippie","fithic")

for (method in methods) 
{
  cis_interactions_intersection<-read.table(paste(dataset,cell_line, samplename, method, "cis_intersection_universe.txt",sep="_"), sep="\t", stringsAsFactors=F, fill=T, header=T)
  cis_interactions_intersection$where<-paste(cis_interactions_intersection$where,",",sep="")
  
  ### order replicates 

  replicates_ordered<-replicates
  replicates_ordered<-paste(replicates_ordered,",",sep="")
  
  JI_matrix<-matrix(rep(NA,length(replicates)^2),nrow=length(replicates))
  rownames(JI_matrix)<-replicates_ordered
  colnames(JI_matrix)<-replicates_ordered

  OC_matrix<-JI_matrix

  pippo<-cis_interactions_intersection$where
  all_comb<-combn(replicates_ordered,2)
  
  for (comb_i in 1:ncol(all_comb)) {

    i<-all_comb[1,comb_i]
    j<-all_comb[2,comb_i]
    
    jacc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/sum(grepl(i,pippo) | grepl(j,pippo))
    oc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/min(c(sum(grepl(i,pippo)),sum(grepl(j,pippo))))

    JI_matrix[i,j]<-jacc_ij
    OC_matrix[i,j]<-oc_ij
    }
  assign(paste("JI_matrix",method,sep="_"), JI_matrix)
  assign(paste("OC_matrix",method,sep="_"), OC_matrix)
 
} #end for methods 

replicates<-replicates_ordered

### calculate inter-enzyme JI

for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  inter_JI_matrix<-JI_matrix[paste(replicates1_ordered,",",sep=""),paste(replicates2_ordered,",",sep="")]

  all_pairwise_JI<-inter_JI_matrix[!is.na(inter_JI_matrix)]
  inter_JI_average<-mean(all_pairwise_JI)
  inter_JI_median<-median(all_pairwise_JI)  

  final_result<-paste(method,inter_JI_average,inter_JI_median,collapse="\t")
  write.table(final_result, paste("Inter-enzyme",dataset,cell_line,"concordance_peaks.txt",sep="_"), sep="\t", quote=F, row.names=F, col.names=F, append=T)
  
  write.table(cbind(method,all_pairwise_JI), paste("Inter-enzyme",dataset,cell_line,"concordance_peaks_all_pairwise_JI.txt",sep="_"),sep="\t",col.names=F, row.names=F, quote=F, append=T)
}

################################################################################
### Supplementary Figure 5A 
### Inter-enzyme JI heatmaps
################################################################################

JI_max_value<-max(c(max(JI_matrix_hiccups,na.rm=T),max(JI_matrix_gothic,na.rm=T),max(JI_matrix_diffhic,na.rm=T),max(JI_matrix_hippie,na.rm=T),max(JI_matrix_homer,na.rm=T),max(JI_matrix_fithic,na.rm=T)))
library(RColorBrewer)

replicates<-gsub(",","",replicates)
pdf(paste("SuppFigure5A.pdf",sep="")

par(mfrow=c(2,2)) 
for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  plot_heatmap_with_legend(JI_matrix, "Jaccard", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Reds", n=9))(100),max_value=JI_max_value, own_scale=F, type="enzyme")
}
for (method in methods) {
  OC_matrix<-get(paste("OC_matrix",method,sep="_"))
  plot_heatmap_with_legend(OC_matrix, "Overlap", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Blues", n=9))(100),max_value=1, own_scale=F, type="enzyme")
}
dev.off()

################################################################################
### Supplementary Figure 5B
### inter-enzyme JI boxplot
################################################################################
ols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")

interazioni<-read.table("Inter-enzyme_Rao_GM12878_concordance_peaks_all_pairwise_JI.txt",sep="\t",stringsAsFactors=F, header=F,fill=T)
colnames(interazioni)<-c("Method","value")
interazioni$Method<-factor(interazioni$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
mxy <- interazioni

pdf("SuppFigure5B.pdf",width=10)
boxplot(mxy$value ~ mxy$Method, col=cols, main="Rao MboI vs DpnII",ylab="JI")
dev.off()
