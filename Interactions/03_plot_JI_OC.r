################################################################################
### plots for JACCARD INDEX
################################################################################

library("RColorBrewer")
library(ggplot2)
library(reshape2)
library(scales)
library(gridExtra)

interazioni<-read.table("peaks_concordance_all_pairwise_JI_fixed.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
interazioni$Method<-factor(interazioni$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))

################################################################################
### Supplementary Figure 3A
### all pairwise JI, for each dataset
################################################################################

selected_subset_few_columns<-interazioni
selected_subset_few_columns$Dataset<-paste(selected_subset_few_columns$Dataset, selected_subset_few_columns$Cell_line)
selected_subset_few_columns$Dataset<-factor(selected_subset_few_columns$Dataset, levels=c("Lieberman-Aiden GM06990","Dixon_2012 H1_hESC","Dixon_2012 IMR90","Jin IMR90","Rao GM12878","Rao IMR90","Dixon_2015 H1_hESC"))
selected_subset_few_columns<-selected_subset_few_columns[,c("Method","Dataset","Type","JI")]
mxy <- selected_subset_few_columns
colnames(mxy)[4]<-"value"

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")

plots<-list()
### create object for cis
mxy_cis<-mxy[mxy$Type=="cis",] 
mxy_cis$value[is.na(mxy_cis$value)]<-0
plots[[1]]<- ggplot(mxy_cis, aes(x=Method, y=value)) + geom_boxplot(fill=rep(cols,7)) + facet_grid(~Dataset) + ggtitle(paste("")) + labs(y="CIS JI") + theme(plot.background = element_blank(),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),axis.title.x = element_blank(),axis.text.x = element_blank(),axis.ticks.x=element_blank(),legend.position="none")  + scale_y_continuous(labels=function(n){format(n, scientific = F)},expand = c(0.05,0)) +theme(plot.margin = unit(c(0.5,0.5,0,0.5), "cm"))

### create object for trans - same max value as cis
mxy_trans<-mxy[mxy$Type=="trans",]
mxy_trans$value[is.na(mxy_trans$value)]<-0
plots[[2]]<- ggplot(mxy_trans, aes(x=Method, y=value)) + geom_boxplot(fill=rep(cols,7)) + facet_grid(~Dataset) + labs(y="TRANS JI")+  theme(plot.background = element_blank(),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),strip.text.x=element_blank(), axis.text.x = element_text(size=8, angle = 45, hjust = 1),legend.position="none")  + scale_y_continuous(labels=function(n){format(n, scientific = F)})+theme(plot.margin = unit(c(0,0.5,0.5,0.5), "cm"))

ml <- marrangeGrob(plots, nrow=2, ncol=1, top=NULL)
ggsave("SuppFigure3A.pdf", ml, width = 13)

################################################################################
### Figure 2D
### all pairwise JI, for all datasets
################################################################################

pdf("Figure2D.pdf",width=10)
par(mfrow=c(1,2))
for (type in c("cis","trans")) {
  selection<-interazioni$Type==type
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$JI ~ selected_subset$Method, col=cols, main=paste(type),ylab="JI", ylim=c(0,0.8))
}
dev.off()

################################################################################
### plots for OVERLAP COEFFICIENT
################################################################################

interazioni<-read.table("peaks_concordance_all_pairwise_OC.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
interazioni$Method<-factor(interazioni$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))

selected_subset_few_columns<-interazioni
selected_subset_few_columns$Dataset<-paste(selected_subset_few_columns$Dataset, selected_subset_few_columns$Cell_line)
selected_subset_few_columns$Dataset<-factor(selected_subset_few_columns$Dataset, levels=c("Lieberman-Aiden GM06990","Dixon_2012 H1_hESC","Dixon_2012 IMR90","Jin IMR90","Rao GM12878","Rao IMR90","Dixon_2015 H1_hESC"))
selected_subset_few_columns<-selected_subset_few_columns[,c("Method","Dataset","Type","OC")]
mxy <- selected_subset_few_columns
colnames(mxy)[4]<-"value"

plots<-list()
### create object for cis
mxy_cis<-mxy[mxy$Type=="cis",] 
mxy_cis$value[is.na(mxy_cis$value)]<-0
plots[[1]]<- ggplot(mxy_cis, aes(x=Method, y=value)) + geom_boxplot(fill=rep(cols,7)) + facet_grid(~Dataset) + ggtitle(paste("")) + labs(y="CIS OC") + theme(plot.background = element_blank(),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),axis.title.x = element_blank(),axis.text.x = element_blank(),axis.ticks.x=element_blank(),legend.position="none")  + scale_y_continuous(labels=function(n){format(n, scientific = F)},expand = c(0.05,0)) +theme(plot.margin = unit(c(0.5,0.5,0,0.5), "cm"))

### create object for trans - same max value as cis
mxy_trans<-mxy[mxy$Type=="trans",]
mxy_trans$value[is.na(mxy_trans$value)]<-0
plots[[2]]<- ggplot(mxy_trans, aes(x=Method, y=value)) + geom_boxplot(fill=rep(cols,7)) + facet_grid(~Dataset) + labs(y="TRANS OC")+  theme(plot.background = element_blank(),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),strip.text.x=element_blank(), axis.text.x = element_text(size=8, angle = 45, hjust = 1),legend.position="none")  + scale_y_continuous(labels=function(n){format(n, scientific = F)})+theme(plot.margin = unit(c(0,0.5,0.5,0.5), "cm"))

################################################################################
### Supplementary Figure 4C
### all pairwise OC, for each datasets
################################################################################

ml <- marrangeGrob(plots, nrow=2, ncol=1, top=NULL)
ggsave("SuppFigure4C.pdf", ml, width = 13)
