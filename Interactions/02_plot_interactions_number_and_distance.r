
interazioni<-read.table("all_datasets_summary.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)

library("RColorBrewer")

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")
names(cols)<-c("hiccups","gothic","homer","diffhic","hippie","fithic")

column_for_x_axis<-"Post.filtering"
name_for_x_axis<-"#reads after filtering"

################################################################################
### Fig. 2A and Supplementary Figure 1A-B
### scatterplot number of interactions vs number of reads retained after filters
################################################################################

pdf("Figure2A_SuppFigure1A_1B",width=10)

par(mfrow=c(2,3))

for (column_for_y_axis in c("cis","trans")) {

  for (resolution in c("1Mb", "40kb","5kb")) {
  
    selection<-interazioni$Resolution==resolution

    selected_subset<-interazioni[selection,]
    selected_subset<-selected_subset[!is.na(selected_subset$interactions),]
    
    selected_subset$Method<-factor(selected_subset$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
    method_list<-c("hiccups","gothic","homer","diffhic","hippie","fithic")
    if (column_for_y_axis=="trans") {
       method_list<-c("gothic","homer","diffhic","hippie")
       selected_subset[selected_subset$Method=="fithic","trans"]<-NA
       selected_subset[selected_subset$Method=="hiccups","trans"]<-NA
    }
    
    ### plot
    plot(log10(selected_subset[,column_for_x_axis]), log10(selected_subset[,column_for_y_axis]), col=cols[selected_subset$Method], pch=16, xlab=name_for_x_axis, ylab=paste("#", column_for_y_axis," interactions",sep=""), main=paste(resolution,"datasets"))
    
    ### lowess smooting
    if (resolution=="5kb") {
    for (i in 1:length(method_list)) {
      method<-method_list[i]
      y<-selected_subset[selected_subset$Method==method,column_for_y_axis]
      x<-selected_subset[selected_subset$Method==method,column_for_x_axis]
      zeros<- x==0 | y==0
      logm1 <- lm(log10(y[!zeros]) ~ log10(x[!zeros]))
      abline(logm1, col=cols[method], lwd=2)
    }
    }

  } # end for on resolution
} # end for on interactions type

dev.off()

################################################################################
### Supplementary Figure 1C
### scatterplot ratio CIS/TRANS vs number of reads retained after filters
################################################################################

resolution <-"5kb"
method_list<-c("gothic","homer","diffhic","hippie")
selection<-interazioni$Resolution==resolution & interazioni$Method %in% method_list
selected_subset<-interazioni[selection,]
selected_subset<-selected_subset[!is.na(selected_subset$interactions),]
selected_subset$Method<-factor(selected_subset$Method, levels=c("gothic","homer","diffhic","hippie"))
cols<-c("#F46D43","#FDAE61","#FEE090","#74ADD1")
names(cols)<-c("gothic","homer","diffhic","hippie")

selected_subset$ratio<-selected_subset$cis/selected_subset$trans
column_for_y_axis<-"ratio"

pdf("SuppFigure1C.pdf")

plot(log10(selected_subset[,column_for_x_axis]), log10(selected_subset[,column_for_y_axis]), col=cols[selected_subset$Method], pch=16, xlab=name_for_x_axis, ylab=paste("#", column_for_y_axis," CIS over TRANS interactions",sep=""), main=paste(resolution,"datasets"))

for (i in 1:length(method_list)) {
  method<-method_list[i]
  y<-selected_subset[selected_subset$Method==method,column_for_y_axis]
  x<-selected_subset[selected_subset$Method==method,column_for_x_axis]
  zeros<- x==0 | y==0 | !is.finite(x) | !is.finite(y)
  logm1 <- lm(log10(y[!zeros]) ~ log10(x[!zeros]))
  abline(logm1, col=cols[method], lwd=2)
}

dev.off()

################################################################################
### Fig. 2B and Supp. Figure 2A
### boxplot of average distances between anchoring points in CIS interactions
################################################################################

pdf("Figure2B_SuppFigure2A.pdf",width=10)

par(mfrow=c(2,3))

for (resolution in c("1Mb", "40kb","5kb")) {

  selection<-interazioni$Resolution==resolution
  
  ### apply filters
  selected_subset<-interazioni[selection,]
  selected_subset<-selected_subset[!is.na(selected_subset$interactions),]
  
  selected_subset$Method<-factor(selected_subset$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
  method_list<-c("hiccups","gothic","homer","diffhic","hippie","fithic")

  ### plot
  if (resolution=="1Mb") {
     boxplot((selected_subset$mean_distance+1) ~ selected_subset$Method, col=cols, main=paste(resolution,"datasets"),ylab="average distance (log))", log="y")
  } else {
     boxplot((selected_subset$mean_distance+1) ~ selected_subset$Method, col=cols, main=paste(resolution,"datasets"), log="y")
  }
} # end for on resolution

for (resolution in c("1Mb", "40kb","5kb")) {

  selection<-interazioni$Resolution==resolution
  
  ### apply filters
  selected_subset<-interazioni[selection,]
  selected_subset<-selected_subset[!is.na(selected_subset$interactions),]
  
  selected_subset$Method<-factor(selected_subset$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
  method_list<-c("hiccups","gothic","homer","diffhic","hippie","fithic")

  ### plot
  if (resolution=="1Mb") {
     boxplot((selected_subset$median_distance+1) ~ selected_subset$Method,col=cols, main=paste(resolution,"datasets"),ylab="median distance (log))", log="y")
  } else {
     boxplot((selected_subset$median_distance+1) ~ selected_subset$Method, col=cols, main=paste(resolution,"datasets"), log="y")
  }
} # end for on resolution
  
dev.off()



