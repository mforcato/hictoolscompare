format_homer<-function(filename, outfilename) {

  peaks<-read.table(filename, header=T, sep='\t', stringsAsFactors=F)
  trans_replicates <- peaks$chr.1. != peaks$chr.2 & peaks$start.1. <  peaks$start.2.
  peaks<- peaks[!trans_replicates,]
  on_chrY<-peaks$chr.1. =="chrY" | peaks$chr.2. == "chrY"
  peaks<- peaks[!on_chrY,]
  formatted_peaks<- peaks[,c(1:5,8:11)]
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")  
  write.table(formatted_peaks, paste(outfilename, "_homer_formatted", ".txt", sep=""), sep="\t", row.names=F, quote=F)
}

format_hiccups<-function(filename, outfilename) {

	if (file.info(filename)$size==0) {return(0)}
  peaks<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  library(tools)
  filename_no_ext<-file_path_sans_ext(basename(filename))
  if(!sum(peaks$chr1==peaks$chr2)==nrow(peaks)) print ("warning not all cis")
  peaks$chr1<-paste("chr",peaks$chr1,sep="")
  peaks$chr2<-paste("chr",peaks$chr2,sep="")
  on_chrY<-peaks$chr1 =="chrY" | peaks$chr2 == "chrY"
  peaks<- peaks[!on_chrY,]  
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(peaks$chr1,peaks$x1,sep="-")
  PeakID_2<-paste(peaks$chr2,peaks$y1,sep="-")    
  formatted_peaks<-data.frame(InteractionID,  PeakID_2, peaks[4:6], PeakID_1, peaks[,1:3], stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  write.table(formatted_peaks, paste(outfilename,"_hiccups_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

format_fithic<-function(filename, outfilename, bin_size) {

  peaks<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  library(tools)
  filename_no_ext<-file_path_sans_ext(basename(filename))
  chr1<-peaks$chr1
  chr2<-peaks$chr2
  halfbin<-bin_size/2
  start1<-as.integer(as.numeric(peaks$fragmentMid1) - halfbin)
  end1<-as.integer(peaks$fragmentMid1 + halfbin)
  start2<-as.integer(peaks$fragmentMid2 - halfbin)
  end2<-as.integer(peaks$fragmentMid2 + halfbin)
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(peaks$chr1,start1,sep="-")
  PeakID_2<-paste(peaks$chr2,start2,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_2, chr2, start2, end2, PeakID_1, chr1, start1, end1, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  write.table(formatted_peaks, paste(outfilename, "_fithic_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

format_gothic<-function(filename, outfilename, bin_size) {

  peaks<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  chr1<-peaks$chr1
  chr2<-peaks$chr2
  start1<-as.integer(peaks$locus1)
  end1<-as.integer(peaks$locus1 + bin_size)
  start2<-as.integer(peaks$locus2)
  end2<-as.integer(peaks$locus2 + bin_size)
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(peaks$chr1,start1,sep="-")
  PeakID_2<-paste(peaks$chr2,start2,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_2, chr2, start2, end2, PeakID_1, chr1, start1, end1, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  write.table(formatted_peaks, paste(outfilename,"_gothic_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

format_hippie_as_bin<-function(filename, outfilename, bin_size) {
  
  peaks<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 
  peaks<-peaks[peaks$V3<0.01,]
  chr1<-do.call(rbind,strsplit(peaks$V1,":"))[,1] 
  chr2<-do.call(rbind,strsplit(peaks$V2,":"))[,1]
  on_chrY<-chr1 =="chrY" | chr2 == "chrY"
  peaks<- peaks[!on_chrY,]
  bin1<-do.call(rbind,strsplit(peaks$V1,":")) 
  bin2<-do.call(rbind,strsplit(peaks$V2,":"))
  chr1<-bin1[,1]
  chr2<-bin2[,1]
  startend1<-do.call(rbind,strsplit(bin1[,2],"-"))
  startend2<-do.call(rbind,strsplit(bin2[,2],"-"))
  start1<-as.integer(startend1[,1]) 
  end1<-as.integer(startend1[,2]) 
  start2<-as.integer(startend2[,1]) 
  end2<-as.integer(startend2[,2]) 
  fragment1_size<-end1 - start1
  fragment2_size<-end2 - start2
  midpoint1<- as.integer(start1 + round(fragment1_size/2))
  midpoint2<- as.integer(start2 + round(fragment2_size/2))

  ### map midpoints to bins
  start1_bin_level <- as.integer((midpoint1 %/% bin_size) * bin_size)
	start2_bin_level <- as.integer((midpoint2 %/% bin_size) * bin_size)	
  bin_size<-as.integer(bin_size)
  end1_bin_level<-start1_bin_level+bin_size
  end2_bin_level<-start2_bin_level+bin_size
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(chr1,start1_bin_level,sep="-")
  PeakID_2<-paste(chr2,start2_bin_level,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_2, chr2, start2_bin_level, end2_bin_level, PeakID_1, chr1, start1_bin_level, end1_bin_level, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  in_same_bin<-formatted_peaks$chr.1==formatted_peaks$chr.2 & formatted_peaks$start.1 == formatted_peaks$start.2
  formatted_peaks<-formatted_peaks[!in_same_bin,]
  doppi<-duplicated(paste(formatted_peaks$PeakID.1,formatted_peaks$PeakID.2))
  formatted_peaks<-formatted_peaks[!doppi,]
  write.table(formatted_peaks, paste(outfilename, "_hippie_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

format_hippie_fragmentLevel<-function(filename, outfilename, bin_size) {
  
  peaks<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 
  peaks<-peaks[peaks$V3<0.01,]
  chr1<-do.call(rbind,strsplit(peaks$V1,":"))[,1] 
  chr2<-do.call(rbind,strsplit(peaks$V2,":"))[,1]
  on_chrY<-chr1 =="chrY" | chr2 == "chrY"
  peaks<- peaks[!on_chrY,]
  bin1<-do.call(rbind,strsplit(peaks$V1,":")) 
  bin2<-do.call(rbind,strsplit(peaks$V2,":"))
  chr1<-bin1[,1]
  chr2<-bin2[,1]
  startend1<-do.call(rbind,strsplit(bin1[,2],"-"))
  startend2<-do.call(rbind,strsplit(bin2[,2],"-"))
  start1<-as.integer(startend1[,1]) 
  end1<-as.integer(startend1[,2]) 
  start2<-as.integer(startend2[,1]) 
  end2<-as.integer(startend2[,2]) 
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(chr1,start1,sep="-")
  PeakID_2<-paste(chr2,start2,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_2, chr2, start2, end2, PeakID_1, chr1, start1, end1, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  write.table(formatted_peaks, paste(outfilename, "_hippie_formatted_fragmentLevel", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}


format_diffhic_as_bin<-function(filename, outfilename, bin_size) {

  peaks<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  too_big <- peaks$width_1 > bin_size*2 | peaks$width_2 > bin_size*2
  peaks<-peaks[!too_big,]    
  start1<-as.integer(peaks$start_1) 
  end1<-as.integer(peaks$end_1) 
  start2<-as.integer(peaks$start_2) 
  end2<-as.integer(peaks$end_2) 
  fragment1_size<-end1 - start1
  fragment2_size<-end2 - start2
  midpoint1<- as.integer(start1 + round(fragment1_size/2))
  midpoint2<- as.integer(start2 + round(fragment2_size/2))
  
  ### map to bins
  start1_bin_level <- as.integer((midpoint1 %/% bin_size) * bin_size)
	start2_bin_level <- as.integer((midpoint2 %/% bin_size) * bin_size)	
  bin_size<-as.integer(bin_size)
  end1_bin_level<-start1_bin_level+bin_size
  end2_bin_level<-start2_bin_level+bin_size
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(peaks$seqnames_1,start1_bin_level,sep="-")
  PeakID_2<-paste(peaks$seqnames_2,start2_bin_level,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_1, peaks$seqnames_1, start1_bin_level, end1_bin_level, PeakID_2, peaks$seqnames_2, start2_bin_level, end2_bin_level, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  in_same_bin<-formatted_peaks$chr.1==formatted_peaks$chr.2 & formatted_peaks$start.1 == formatted_peaks$start.2
  formatted_peaks<-formatted_peaks[!in_same_bin,]
  doppi<-duplicated(paste(formatted_peaks$PeakID.1,formatted_peaks$PeakID.2))
  formatted_peaks<-formatted_peaks[!doppi,]
  write.table(formatted_peaks, paste(outfilename, "_diffhic_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

summary_from_file<-function(filename, outfilename="summary.txt") {

  if (!file.exists(filename)) {
     final_result<-paste(c(filename,0,0,0,NA,NA),collapse="\t")
     write.table(final_result, "./compare_peaks/peaks_summary.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
     return("no interactions")
  }	
  file1<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  num_filename1<-nrow(file1)
  cis_file1<-file1[file1$chr.1 == file1$chr.2,]
  num_cis1<-nrow(cis_file1)
  num_trans1<- num_filename1 - num_cis1
  mean_dist1<-as.integer(mean(cis_file1$start.1 - cis_file1$end.2))
  median_dist1<-as.integer(median(cis_file1$start.1 - cis_file1$end.2))
  final_result<-paste(c(filename,num_filename1,num_cis1,num_trans1,mean_dist1,median_dist1),collapse="\t")
  write.table(final_result, "./compare_peaks/peaks_summary.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T) 
  print(paste(filename,num_filename1,num_cis1,num_trans1,mean_dist1,median_dist1))
}

compare_interactions_with_string_approach<-function(cell_line, samplename, replicates, bin_size, method, threshold=2, write_universe=F, quit_after_writing=F, outfilename=NULL) {

  require(plyr)
  if (is.null(outfilename)) outfilename<-paste(dataset,cell_line, samplename, method ,sep="_")
  cis_interactions_union<-NULL
  trans_interactions_union<-NULL
  for (rep_i in replicates) {
  
    peaks1<-read.table(paste(cell_line,samplename,rep_i,method,"formatted.txt",sep="_"), header=T, sep='\t', stringsAsFactors=F, fill=T) 
    cis_peaks1<-peaks1[peaks1$chr.1 == peaks1$chr.2,]
    cis_interactions<-cbind(paste(cis_peaks1$PeakID.1,cis_peaks1$PeakID.2,sep="-"),rep(rep_i,nrow(cis_peaks1)))
    cis_interactions_union<-rbind(cis_interactions_union,cis_interactions)
    
    if (!method %in% c("fithic","hiccups")) {
      trans_peaks1<-peaks1[peaks1$chr.1 != peaks1$chr.2,]
      trans_interactions<-cbind(paste(trans_peaks1$PeakID.1,trans_peaks1$PeakID.2,sep="-"),rep(rep_i,nrow(trans_peaks1)))
      trans_interactions_union<-rbind(trans_interactions_union,trans_interactions)
    } # end if
  } # end for
  
  cis_interactions_union<-as.data.frame(cis_interactions_union, stringsAsFactors=F)
  cis_interactions_intersection<-ddply(cis_interactions_union, .(V1), summarize, where = paste(V2, collapse=","), number=length(V2))
  
  if (write_universe) {
    write.table(cis_interactions_intersection, paste(outfilename, "cis_intersection_universe.txt",sep="_"), sep="\t", quote=F, row.names=F)
  }

  if (!method %in% c("fithic","hiccups")) {
    trans_interactions_union<-as.data.frame(trans_interactions_union, stringsAsFactors=F)
    trans_interactions_intersection<-ddply(trans_interactions_union, .(V1), summarize, where = paste(V2, collapse=","), number=length(V2))
    if (write_universe) {
      write.table(trans_interactions_intersection, paste(outfilename, "trans_intersection_universe.txt",sep="_"), sep="\t", quote=F, row.names=F)
    }
  }
  
  if (quit_after_writing) return()    
  print(table(cis_interactions_intersection$number))
  inatleast2<-cis_interactions_intersection[cis_interactions_intersection$number >= threshold,]  
  
  ### back to simple format CIS
  entry<-do.call(rbind,strsplit(inatleast2$V1,"-")) 
  InteractionID<-paste("interaction",1:nrow(entry), sep="")
  PeakID_1<-paste(entry[,1],entry[,2],sep="-")
  PeakID_2<-paste(entry[,3],entry[,4],sep="-")
  bin_size<-as.integer(bin_size)
  formatted_peaks<-data.frame(InteractionID, PeakID_1, entry[,1], as.integer(entry[,2]), as.integer(entry[,2])+bin_size, PeakID_2, entry[,3], as.integer(entry[,4]), as.integer(entry[,4])+bin_size, stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")  
  write.table(formatted_peaks,paste(outfilename,"cis_common.txt",sep="_"), quote=F, row.names=F, sep="\t")
  
  if (!method %in% c("fithic","hiccups")) {
    
    print(table(trans_interactions_intersection$number))
    inatleast2<-trans_interactions_intersection[trans_interactions_intersection$number >= threshold,]
    ### back to simple format TRANS
    entry<-do.call(rbind,strsplit(inatleast2$V1,"-")) 
    InteractionID<-paste("interaction",1:nrow(entry), sep="")
    PeakID_1<-paste(entry[,1],entry[,2],sep="-")
    PeakID_2<-paste(entry[,3],entry[,4],sep="-")
    bin_size<-as.integer(bin_size)
    formatted_peaks<-data.frame(InteractionID, PeakID_1, entry[,1], as.integer(entry[,2]), as.integer(entry[,2])+bin_size, PeakID_2, entry[,3], as.integer(entry[,4]), as.integer(entry[,4])+bin_size, stringsAsFactors=F)
    colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")  
    write.table(formatted_peaks,paste(outfilename,"trans_common.txt",sep="_"), quote=F, row.names=F, sep="\t")
  } #end if
}

calculate_concordance<-function(dataset, cell_line, samplename, replicates, method, type="cis", threshold=2) { 

  cis_interactions_intersection<-read.table(paste(dataset,cell_line, samplename, method, type, "intersection_universe.txt",sep="_"), sep="\t", stringsAsFactors=F, fill=T, header=T)
  cis_interactions_intersection$where<-paste(cis_interactions_intersection$where,",",sep="")
  replicates_ordered<-replicates
  replicates_ordered<-paste(replicates_ordered,",",sep="")
  JI_matrix<-matrix(rep(NA,length(replicates)^2),nrow=length(replicates))
  rownames(JI_matrix)<-replicates_ordered
  colnames(JI_matrix)<-replicates_ordered
  OC_matrix<-JI_matrix

  pippo<-cis_interactions_intersection$where
  all_comb<-combn(replicates_ordered,2)
  
  for (comb_i in 1:ncol(all_comb)) {

    i<-all_comb[1,comb_i]
    j<-all_comb[2,comb_i]
    
    jacc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/sum(grepl(i,pippo) | grepl(j,pippo))
    oc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/min(c(sum(grepl(i,pippo)),sum(grepl(j,pippo))))

    JI_matrix[i,j]<-jacc_ij
    OC_matrix[i,j]<-oc_ij

    }
 
  final_interactions<-sum(cis_interactions_intersection$number >= threshold)
  
  ### JI  
  all_pairwise_JI<-JI_matrix[!is.na(JI_matrix)]
  JI_total<-final_interactions/nrow(cis_interactions_intersection)
  JI_average<-mean(all_pairwise_JI)
  JI_median<-median(all_pairwise_JI)
  final_result<-paste(dataset, cell_line, samplename, method, type, final_interactions, round(JI_total,5), round(JI_average,5), round(JI_median,5),collapse="\t")
  write.table(final_result, "./compare_peaks/concordance/peaks_concordance_summary_JI.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  write.table(cbind(dataset,cell_line,method,type,all_pairwise_JI), "./compare_peaks/concordance/peaks_concordance_all_pairwise_JI.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  
  ### OC
  all_pairwise_OC<-OC_matrix[!is.na(OC_matrix)]
  OC_total<-final_interactions/nrow(cis_interactions_intersection)
  OC_average<-mean(all_pairwise_OC)
  OC_median<-median(all_pairwise_OC)
  final_result<-paste(dataset, cell_line, samplename, method, type, final_interactions, round(OC_total,5), round(OC_average,5), round(OC_median,5),collapse="\t")
  write.table(final_result, "./compare_peaks/concordance/peaks_concordance_summary_OC.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  write.table(cbind(dataset,cell_line,method,type,all_pairwise_OC), "./compare_peaks/concordance/peaks_concordance_all_pairwise_OC.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
}


split_single_replicate<-function(cell_line, samplename, replicates, method)     {
  
  peaks1<-read.table(paste(cell_line,samplename,replicates,method,"formatted.txt",sep="_"), header=T, sep='\t', stringsAsFactors=F, fill=T) 
  cis_peaks1<-peaks1[peaks1$chr.1 == peaks1$chr.2,]
  write.table(cis_peaks1,paste(dataset,cell_line,samplename,method,"cis_common.txt",sep="_"), quote=F, row.names=F, sep="\t")
  if (!method %in% c("fithic","hiccups")) {
    trans_peaks1<-peaks1[peaks1$chr.1 != peaks1$chr.2,]
    write.table(trans_peaks1,paste(dataset,cell_line,samplename,method,"trans_common.txt",sep="_"), quote=F, row.names=F, sep="\t")
  }
}

compare_with_chromatin_states<-function(dataset, cell_line, samplename, method, interactions_type="cis", write.annotated.files=T, min_overlap=50) {

  working_dir<-"./compare_peaks"
  filename<-file.path(working_dir,dataset,cell_line, paste(dataset,cell_line,samplename,method,interactions_type,"common.txt", sep="_"))
  annotated_interaction<-read.table(filename, header=T, sep='\t', stringsAsFactors=F, fill=T)
  library(ChIPpeakAnno)
  interaction_bin1_RD <-BED2RangedData(data.frame(annotated_interaction[,c(3:5)],paste(annotated_interaction[,1],"bin1",sep="_"),stringsAsFactors=F))
  interaction_bin2_RD <-BED2RangedData(data.frame(annotated_interaction[,c(7:9)],paste(annotated_interaction[,1],"bin2",sep="_"),stringsAsFactors=F))
  
  if (cell_line == "H1_hESC")  load("./compare_peaks/ChromatinStates/E003_merged_sorted.RData")
  if (cell_line == "IMR90")  load("./compare_peaks/ChromatinStates/E017_merged_sorted.RData") 
  if (cell_line == "GM12878" | cell_line == "GM06990") load("./compare_peaks/ChromatinStates/E116_merged_sorted.RData")
  if (cell_line == "FlyEmbryo") load("./compare_peaks/ChromatinStates/fly_EL_merged_sorted.RData")
  
  overlappingPeaks_bin1 <- findOverlappingPeaks(interaction_bin1_RD, states_RD, minoverlap=min_overlap)$OverlappingPeaks
  overlappingPeaks_bin2 <- findOverlappingPeaks(interaction_bin2_RD, states_RD, minoverlap=min_overlap)$OverlappingPeaks
  
  overlapping_bin1<-data.frame("interaction"=as.character(overlappingPeaks_bin1$TF1), "state"=as.character(overlappingPeaks_bin1$TF2), stringsAsFactors=F)
  overlapping_bin2<-data.frame("interaction"=as.character(overlappingPeaks_bin2$TF1), "state"=as.character(overlappingPeaks_bin2$TF2), stringsAsFactors=F)
  
  overlapping_bin1$state<-do.call(rbind,strsplit(as.vector(overlapping_bin1$state),"_"))[,1]
  overlapping_bin2$state<-do.call(rbind,strsplit(as.vector(overlapping_bin2$state),"_"))[,1]
    
  require(plyr)
  overlapping_bin1 <- ddply(overlapping_bin1, .(interaction), summarize,  state=paste(unique(sort(state)),collapse=";"))
  overlapping_bin2 <- ddply(overlapping_bin2, .(interaction), summarize,  state=paste(unique(sort(state)),collapse=";"))
  
  overlapping_bin1$interaction<-do.call(rbind,strsplit(as.vector(overlapping_bin1$interaction),"_"))[,1]
  overlapping_bin2$interaction<-do.call(rbind,strsplit(as.vector(overlapping_bin2$interaction),"_"))[,1]
  interactions_with_states<-merge(overlapping_bin1, overlapping_bin2, by="interaction", all.x=T)
  
  annotated_interactions_with_states<-merge(annotated_interaction, interactions_with_states, by.x="InteractionID", by.y="interaction", all.x=T)
  
  interaction_type<-cbind(annotated_interactions_with_states$state.x, annotated_interactions_with_states$state.y)
  pippo<-apply(interaction_type, 1, sort)
  if (is.list(pippo)) interaction_type_sorted <- t(sapply(pippo, function(x,m) c(x, rep(NA, m-length(x))), max(rapply(pippo,length))))  else  interaction_type_sorted <- t(pippo)
  
  if (write.annotated.files) {
     write.table(annotated_interactions_with_states, file.path(working_dir,dataset,cell_line, paste(dataset,cell_line,samplename,method,interactions_type,"common.annotated_with_states.txt",sep="_")), sep="\t", quote=F, row.names=F)
     write.table(table(interaction_type_sorted[,1], interaction_type_sorted[,2]), file.path(working_dir,dataset,cell_line, paste(dataset,cell_line,samplename,method,interactions_type,"common.annotated_with_states_table.txt",sep="_")), sep="\t", quote=F)
  }
  
  ### get percentage of classes of interactions
  
  row.has.na <- apply(interaction_type_sorted, 1, function(x){any(is.na(x))})
  interaction_type_sorted_notna<-interaction_type_sorted[!row.has.na,]
  
  ### not expected
  not_plausible<-sum(table(paste(interaction_type_sorted[,1], interaction_type_sorted[,2]))[c("Enh HET", "Enh;TSS HET", "HET TSS")], na.rm=T)
  not_plausible_percent<-not_plausible/nrow(interaction_type_sorted) *100
  not_plausible_percent_notna<-not_plausible/nrow(interaction_type_sorted_notna) *100
  
  ### promoter - enhancer
  prom_enh<-which(grepl("TSS",interaction_type_sorted[,1])& grepl("Enh",interaction_type_sorted[,2]))
  enh_prom<-which(grepl("TSS",interaction_type_sorted[,2])& grepl("Enh",interaction_type_sorted[,1]))
  pron_enh<-length(unique(c(prom_enh,enh_prom)))
  pron_enh_percent<-pron_enh/nrow(interaction_type_sorted) *100
  pron_enh_percent_notna<-pron_enh/nrow(interaction_type_sorted_notna) *100
  
  ### Heterochromatin/quiescent - heterochromatin/quiescent
  het_het<-sum(table(paste(interaction_type_sorted[,1], interaction_type_sorted[,2]))[c("HET HET")], na.rm=T)
  het_het_percent<-het_het/nrow(interaction_type_sorted) *100
  het_het_percent_notna<-het_het/nrow(interaction_type_sorted_notna) *100                                                                                                                         
  
  entry<-paste(dataset,cell_line,samplename,interactions_type,method, nrow(interaction_type_sorted),nrow(interaction_type_sorted_notna),not_plausible,not_plausible_percent,not_plausible_percent_notna,pron_enh,pron_enh_percent,pron_enh_percent_notna, het_het, het_het_percent,het_het_percent_notna,collapse="\t")
  write.table(entry, file.path(working_dir,dataset,cell_line,paste(dataset,cell_line,samplename,"interaction_classification_vs_chromatinStates.txt",sep="_")),sep="\t",quote=F,col.names=F,row.names=F,append=T)
}

format_interactionFile<-function(filename) {

  peaks<-read.table(filename, header=F, sep='\t', stringsAsFactors=F, fill=T) 
  library(tools)
  filename_no_ext<-file_path_sans_ext(basename(filename))
  if (sum(peaks$V1==peaks$V4)!=nrow(peaks)) print("warning: there are some trans interactions!")
  peaks<-peaks[peaks$V1==peaks$V4,]

  ### it works only if all interactions are in cis
  peaks_ordered<-peaks[peaks$V2>peaks$V5,]
  peaks_wrong<-peaks[peaks$V2<peaks$V5,c(4:6,1:3)]
  colnames(peaks_wrong)<-colnames(peaks_ordered)
  peaks<-rbind(peaks_ordered,peaks_wrong)
  InteractionID<-paste("interaction",1:nrow(peaks), sep="")
  PeakID_1<-paste(peaks$V1,peaks$V2,sep="-")
  PeakID_2<-paste(peaks$V4,peaks$V5,sep="-")
  formatted_peaks<-data.frame(InteractionID, PeakID_1, peaks[,1:3], PeakID_2, peaks[4:6], stringsAsFactors=F)
  colnames(formatted_peaks)<-c("InteractionID","PeakID.1","chr.1","start.1","end.1","PeakID.2","chr.2","start.2","end.2")
  write.table(formatted_peaks, paste(filename_no_ext,"_formatted", ".txt", sep=""), sep="\t", quote=F, row.names=F)
}

compare_interactions_with_TP<-function(filename1, filename1_label="5C", filename2, cell_line, bin_size, method, rep="common", print_found_TP=F) {

  library(ChIPpeakAnno)
  dataset<- strsplit(filename2,"/",fixed=T)[[1]][6]

  bin_size<- as.integer(bin_size)
	
  peaks1<-read.table(filename1, header=T, sep='\t', stringsAsFactors=F, fill=T) 
  peaks2<-read.table(filename2, header=T, sep='\t', stringsAsFactors=F, fill=T) 

  peaks1_bin1_RD <-BED2RangedData(data.frame(peaks1[,c(3:5)],paste(peaks1[,1],"TP",sep="_"),stringsAsFactors=F))
  peaks1_bin2_RD <-BED2RangedData(data.frame(peaks1[,c(7:9)],paste(peaks1[,1],"TP",sep="_"),stringsAsFactors=F))
  
  peaks2_bin1_RD <-BED2RangedData(data.frame(peaks2[,c(3:5)],paste(peaks2[,1],"HiC",sep="_"),stringsAsFactors=F))
  peaks2_bin2_RD <-BED2RangedData(data.frame(peaks2[,c(7:9)],paste(peaks2[,1],"HiC",sep="_"),stringsAsFactors=F))
  
  overlappingPeaks_bin1 <- findOverlappingPeaks(peaks1_bin1_RD, peaks2_bin1_RD, minoverlap=2)$OverlappingPeaks
  overlappingPeaks_bin2 <- findOverlappingPeaks(peaks1_bin2_RD, peaks2_bin2_RD, minoverlap=2)$OverlappingPeaks

  overlapping_bin1<-data.frame("TP"=as.character(overlappingPeaks_bin1$TF1), "HiC"=as.character(overlappingPeaks_bin1$TF2), stringsAsFactors=F)
  overlapping_bin2<-data.frame("TP"=as.character(overlappingPeaks_bin2$TF1), "HiC"=as.character(overlappingPeaks_bin2$TF2), stringsAsFactors=F)
  
  num_filename1<-nrow(peaks1)
  
  common_in_filename1<- overlapping_bin1[paste(overlapping_bin1$TP,overlapping_bin1$HiC,sep="-") %in% paste(overlapping_bin2$TP,overlapping_bin2$HiC,sep="-"),]
  num_common_in_filename1<-length(unique(common_in_filename1$TP))
  if(print_found_TP) {
    valori_final<-c(dataset, cell_line, rep, method, filename1_label, unique(common_in_filename1$TP))
    write.table(paste(valori_final,collapse="\t"), "./compare_peaks/peaks_vs_TP_extended.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
  }
  
  num_filename1_not_adj<-sum(!grepl("ADJ",peaks1$InteractionID))
  num_common_in_filename1_not_adj<-sum(!grepl("ADJ",unique(common_in_filename1$TP)))

  valori_final<-c(dataset, cell_line, rep, method, filename1_label, num_filename1, num_common_in_filename1, num_filename1_not_adj, num_common_in_filename1_not_adj)
  write.table(paste(valori_final,collapse="\t"), "./compare_peaks/peaks_vs_TP.txt",sep="\t",col.names=F, row.names=F, quote=F, append=T)
}


plot_heatmap_with_legend<-function(JI_matrix, key.xlab="score", axes.labels, main.title, heatmap.colors, max_value=1, own_scale=F, type="enzyme"){

  scale01 <- function(x, low = min(x), high = max(x)) {
      x <- (x - low)/(high - low)
      x
  }
  lhei <- c(1, 4)
  lwid <- c(1.5, 4)
  lmat <- rbind(4:3, 2:1)
  lmat <- rbind(lmat[1, ] + 1, c(NA, 1), lmat[2, ] + 1)
  lhei <- c(lhei[1], 0.2, lhei[2])
        
  lmat[is.na(lmat)] <- 0
  margins = c(5, 5)
  layout(lmat, widths = lwid, heights = lhei, respect = FALSE)
  if (type=="enzyme") ColSideColors<- c(rep("darkgreen",5), rep("darkmagenta",11)) else ColSideColors<- c(rep("darkgreen",7), rep("darkmagenta",6)) 

  par(mar = c(0.5, 0, 0, margins[2]))
  image(cbind(1:ncol(JI_matrix)), col = ColSideColors, axes = FALSE)

  par(mar = c(5, 0, 2, 5))
  if (own_scale) {

    if (type=="enzyme") {
    image(JI_matrix, col=heatmap.colors,  main="DpnII                                         MboI", axes=F)  } else {
    image(JI_matrix, col=heatmap.colors,  main="  Rao                                       Jin", axes=F)
    }
  } else {                                 
      if (type=="enzyme") {
    image(JI_matrix, col=heatmap.colors,  main="DpnII                                         MboI", axes=F, zlim=c(0,max_value))  } else {
    image(JI_matrix, col=heatmap.colors,  main="  Rao                                       Jin", axes=F, zlim=c(0,max_value))
    }

  }
  grey_matrix<-matrix(rep(NA,length(axes.labels)^2),nrow=length(axes.labels))
  diag(grey_matrix)<-1
  image(grey_matrix, col="grey",  add=T, axes=F)
  axis(1,at=seq(0,1,length.out=length(axes.labels)),labels=axes.labels,cex.axis=0.8,las=2)
  axis(2,at=seq(0,1,length.out=length(axes.labels)),labels=axes.labels,cex.axis=0.8,las=2)
   
  plot.new()
  plot.new()   
  title(method, cex.main = 1.5)
  mar <- c(5, 4, 2, 1)
  mar[1] <- 2
  par(mar = mar, cex = 0.75, mgp = c(2, 1, 0))
  
  col<-heatmap.colors
  min.raw <- 0
  if (own_scale) max.raw <- max(JI_matrix, na.rm = TRUE) else  max.raw <- max_value
  breaks <- length(col) + 1
  breaks <- seq(min.raw, max.raw, length = breaks)
  tmpbreaks <- breaks
  z <- seq(min.raw, max.raw, length = length(col))
  image(z = matrix(z, ncol = 1), col = col, breaks = tmpbreaks, xaxt = "n", yaxt = "n")
  
  par(usr = c(0, 1, 0, 1))
  lv <- pretty(breaks)
  xv <- scale01(as.numeric(lv), min.raw, max.raw)
  xargs <- list(at = xv, labels = lv, cex.axis=0.8)
  xargs$side <- 1
  do.call(axis, xargs)
  key.xlab <- key.xlab
  mtext(side = 1, key.xlab, line = par("mgp")[1], padj = 0.5, cex=0.7)
}
