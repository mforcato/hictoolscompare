
base_dir<-"./Hi-C_dataset"
cell_line<-"GM06990"
dataset<-"Lieberman-Aiden" 
samplename<-"Hs_HindIII"
#samplename<-"Hs_NcoI"
bin_size<-1000000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

### convert to simple format and get descriptive statistics

### homer
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {

  homer_rep<-file.path(base_dir,dataset,"homer",paste(cell_line,samplename,rep,"homer.txt",sep="_"))
  format_homer(homer_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"homer_formatted.txt",sep="_"))

}

### fithic
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {

  fithic_rep<-file.path(base_dir,dataset,"fithic",paste(cell_line,samplename,rep,"fithic.txt",sep="_"))
  format_fithic(fithic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"fithic_formatted.txt",sep="_"))

}

### gothic
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {
              
  gothic_rep<-file.path(base_dir,dataset,"gothic",paste(cell_line,samplename,rep,"gothic.txt",sep="_"))
  format_gothic(gothic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"gothic_formatted.txt",sep="_"))

}

### diffhic
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {
              
  diffhic_rep<-file.path(base_dir,dataset,"diffhic",paste(cell_line,samplename,rep,"diffhic.txt",sep="_"))
  format_diffhic_as_bin(diffhic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"diffhic_formatted.txt",sep="_"))

}

### hippie
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {
              
  hippie_rep<-file.path(base_dir,dataset,"hippie",paste(cell_line,samplename,rep,"hippie.txt"),sep="_")
  format_hippie_as_bin(hippie_rep, paste(cell_line,samplename,rep,sep="_"),bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"hippie_formatted.txt",sep="_"))

}

### hiccups
if (samplename=="Hs_NcoI") replicates<-c("rA_merge") else replicates<-c("rA_l1", "rArepeat_l2", "rB_merge")
for (rep in replicates) {
              
  hiccups_rep<-file.path("./compare_peaks/HiCCUPS",dataset,paste(samplename,rep,sep="_"),"hiccups",paste("postprocessed_pixels",as.integer(bin_size),sep="_"))
  format_hiccups(hiccups_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"hiccups_formatted.txt",sep="_"))

}

################################################################################
###  compare replicates

base_dir<-"./Hi-C_dataset"
cell_line<-"GM06990"
dataset<-"Lieberman-Aiden" 
bin_size<-1000000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

samplename<-"Hs"
replicates<-c("NcoI_rA_merge","HindIII_rA_l1", "HindIII_rArepeat_l2", "HindIII_rB_merge")

methods<-c("homer","fithic","gothic","diffhic","hiccups")
for (method in methods) compare_interactions_with_string_approach(cell_line, samplename, replicates, bin_size, method, write_universe=T, quit_after_writing=F)
for (method in "hippie") compare_interactions_with_string_approach(cell_line, samplename, c("NcoI_rA_merge","HindIII_rA_l1","HindIII_rB_merge"), bin_size, method, write_universe=T, quit_after_writing=F)

### Jaccard Index
samplename<-"Hs"
replicates<-c("NcoI_rA_merge","HindIII_rA_l1", "HindIII_rArepeat_l2", "HindIII_rB_merge")
methods<-c("homer","fithic","gothic","diffhic","hiccups")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="cis", threshold=2)
methods<-c("homer","gothic","diffhic")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="trans", threshold=2)
replicates<-c("NcoI_rA_merge","HindIII_rA_l1", "HindIII_rB_merge")
calculate_concordance(dataset, cell_line, samplename, replicates, "hippie", type="trans", threshold=2)
 
################################################################################
### compare common interactions with chromatin states

cell_line<-"GM06990"
dataset<-"Lieberman-Aiden" 
samplename<-"Hs"

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

methods<-c("homer","gothic","diffhic")
for (method in methods) {
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="cis")
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="trans")
}
compare_with_chromatin_states(dataset, cell_line, samplename, "fithic", interactions_type="cis")
compare_with_chromatin_states(dataset, cell_line, samplename, "hiccups", interactions_type="cis")

