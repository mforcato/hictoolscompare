
################################################################################
### interactions vs chromatin states
################################################################################

library(RColorBrewer)
library(ggplot2)
library(reshape2)
library(scales)
library(gridExtra)

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")
interazioni<-read.table("all_datasets_comparison_with_chromatin_states.txt",sep="\t",stringsAsFactors=F, header=T,fill=T)
interazioni$Method<-factor(interazioni$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))

################################################################################
### Figure 2E and Supp. Figure 7A
### cis interactions vs chromatin states (5kb datasets)
################################################################################

pdf("Figure2E_SuppFigure7A.pdf",width=10)

par(mfrow=c(1,2))

  ### percent_prom.enh
  
  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2015","Jin","Rao")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_prom.enh ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="percent_prom.enh", ylim=c(0,60))  
  boxplot(selected_subset$num_prom.enh ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="num_prom.enh")  

  ### percent_Het.Het
  
  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2015","Jin","Rao")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_Het.Het ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="percent_Het.Het", ylim=c(0,60))
  boxplot(selected_subset$num_Het.Het ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="num_Het.Het")

  ### not.plausible
  
  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2015","Jin","Rao")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_not.plausible ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="percent_not.plausible", ylim=c(0,60))
  boxplot(selected_subset$num_not.plausible ~ selected_subset$Method, col=cols, main=paste("5kb"),ylab="num_not.plausible")

dev.off()   

################################################################################
### Supp. Figure 7B
### cis interactions vs chromatin states (40kb datasets)
################################################################################

pdf("SuppFigure7B.pdf",width=10)

par(mfrow=c(1,2))

  ### percent_prom.enh

  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2012","Sexton")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_prom.enh ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="percent_prom.enh", ylim=c(0,80))
  boxplot(selected_subset$num_prom.enh ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="num_prom.enh")
  
  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2012","Sexton")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_Het.Het ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="percent_Het.Het", ylim=c(0,80))
  boxplot(selected_subset$num_Het.Het ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="num_Het.Het")

  type <-"cis" 
  selection<-interazioni$interaction.type==type & interazioni$Dataset %in% c("Dixon_2012","Sexton")
  selected_subset<-interazioni[selection,]
  boxplot(selected_subset$percent_not.plausible ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="percent_not.plausible", ylim=c(0,80))
  boxplot(selected_subset$num_not.plausible ~ selected_subset$Method, col=cols, main=paste("40kb"),ylab="num_not.plausible")

dev.off() 
