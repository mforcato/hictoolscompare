
base_dir<-"./Hi-C_dataset"
cell_line<-"FlyEmbryo"
dataset<-"Sexton" 
samplename<-"Dm_DeepSeq_DpnII"
bin_size<-40000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

### convert to simple format and get descriptive statistics

### homer
replicates<-c("rA_merge")
for (rep in replicates) {

  homer_rep<-file.path(base_dir,dataset,"homer",paste(cell_line,samplename,rep,"homer.txt",sep="_"))
  format_homer(homer_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"homer_formatted.txt",sep="_"))

}

### fithic
replicates<-c("rA_merge")
for (rep in replicates) {

  fithic_rep<-file.path(base_dir,dataset,"fithic",paste(cell_line,samplename,rep,"fithic.txt",sep="_"))
  format_fithic(fithic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"fithic_formatted.txt",sep="_"))

}

### gothic
replicates<-c("rA_merge")
for (rep in replicates) {
              
  gothic_rep<-file.path(base_dir,dataset,"gothic",paste(cell_line,samplename,rep,"gothic.txt",sep="_"))
  format_gothic(gothic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"gothic_formatted.txt",sep="_"))

}

### diffhic
replicates<-c("rA_merge")
for (rep in replicates) {
              
  diffhic_rep<-file.path(base_dir,dataset,"diffhic",paste(cell_line,samplename,rep,"diffhic.txt",sep="_"))
  format_diffhic_as_bin(diffhic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"diffhic_formatted.txt",sep="_"))

}

### hippie
replicates<-c("rA_merge")
for (rep in replicates) {
              
  hippie_rep<-file.path(base_dir,dataset,"hippie",paste(cell_line,samplename,rep,"hippie.txt"),sep="_")
  format_hippie_as_bin(hippie_rep, paste(cell_line,samplename,rep,sep="_"),bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"hippie_formatted.txt",sep="_"))

}

### hiccups
replicates<-c("rA_merge")                                                        
for (rep in replicates) {
              
  hiccups_rep<-file.path("./compare_peaks/HiCCUPS",dataset,"embryo_DpnII_aln","hiccups_40kb",paste("postprocessed_pixels",as.integer(bin_size),sep="_"))
  format_hiccups(hiccups_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"hiccups_formatted.txt",sep="_"))

}

################################################################################
### need to split cis and trans
replicates<-c("rA_merge")
methods<-c("homer","fithic","gothic","diffhic", "hippie","hiccups")
for (method in methods) split_single_replicate(cell_line, samplename, replicates, method)

################################################################################
### compare common interactions with chromatin states

cell_line<-"FlyEmbryo"
dataset<-"Sexton" 
samplename<-"Dm_DeepSeq_DpnII"


setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

methods<-c("homer","gothic","diffhic","hippie")
for (method in methods) {
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="cis")
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="trans")
}
compare_with_chromatin_states(dataset, cell_line, samplename, "fithic", interactions_type="cis")
compare_with_chromatin_states(dataset, cell_line, samplename, "hiccups", interactions_type="cis")



################################################################################
### compare with TP
source("./compare_peaks/00_utils_interactions.r")

working_dir<-"./compare_peaks"
setwd(working_dir)

cell_line<-"FlyEmbryo"
dataset<-"Sexton" 
samplename<-"Dm_DeepSeq_DpnII"
bin_size<-40000

methods<-c("homer","gothic","diffhic","hippie", "fithic","hiccups")
replicates<-c("rA_merge")

TP_list<-file.path("./compare_peaks/TP", paste("Sexton_3C_Kc167_TP_formatted_",bin_size,".txt",sep=""))
TP_name<-"Sexton_3C_TP"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
}

TP_list<-file.path("./compare_peaks/TP", paste("Hou_3C_Kc167_TP_formatted_",bin_size,".txt",sep=""))
TP_name<-"Hou_3C_TP"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
}

