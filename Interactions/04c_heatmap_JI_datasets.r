###############################################################################
### heatmaps for inter-dataset JACCARD INDEX
################################################################################

basedir<-"./compare_peaks"
cell_line<-"IMR90"
bin_size<-5000
setwd(file.path(basedir))
source("./compare_peaks/00_utils_interactions.r")

dataset1<-"Rao" 
samplename1<-"Hs_InSitu_MboI"
replicates1<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")

dataset2<-"Jin" 
samplename2<-"Hs_HindIII"
replicates2<-c("rA_merge","rB_merge","rC_merge","rD_merge","rE_l1","rF_l1")

methods<-c("hiccups","homer","gothic","diffhic","hippie","fithic")

### read statistics to order replicates
stats<-read.table("./compare_peaks/all_datasets_summary.txt", sep="\t", stringsAsFactors=F, header=T, fill=T)
stats1<-stats[stats$Dataset==dataset1 & stats$Cell_line==cell_line & stats$Method=="homer",]  # take just one method as an example
replicates1_ordered<-stats1$Replicate[order(stats1$Total_reads,decreasing=T)]
stats2<-stats[stats$Dataset==dataset2 & stats$Cell_line==cell_line & stats$Method=="homer",]
replicates2_ordered<-stats2$Replicate[order(stats2$Total_reads,decreasing=T)]


for (method in methods) 
{
  cis_interactions_intersection1<-read.table(file.path(basedir,dataset1,cell_line,paste(dataset1,cell_line, samplename1, method, "cis_intersection_universe.txt",sep="_")), sep="\t", stringsAsFactors=F, fill=T, header=T)
  cis_interactions_intersection2<-read.table(file.path(basedir,dataset2,cell_line,paste(dataset2,cell_line, samplename2, method, "cis_intersection_universe.txt",sep="_")), sep="\t", stringsAsFactors=F, fill=T, header=T)
  
  library(plyr)
  all_interactions<-rbind(cis_interactions_intersection1,cis_interactions_intersection2)
  all_interactions_unique<-ddply(all_interactions, .(V1), summarize, where = paste(where, collapse=","))

  all_interactions_unique$where<-paste(all_interactions_unique$where,",",sep="")
  
  ### order replicates in terms of decreasing number of interactions
  replicates_ordered<-c(replicates1_ordered,replicates2_ordered)
  replicates_ordered<-paste(replicates_ordered,",",sep="")

  
  JI_matrix<-matrix(rep(NA,length(replicates_ordered)^2),nrow=length(replicates_ordered))
  rownames(JI_matrix)<-replicates_ordered
  colnames(JI_matrix)<-replicates_ordered

  OC_matrix<-JI_matrix

  pippo<-all_interactions_unique$where
  all_comb<-combn(replicates_ordered,2)
  
  for (comb_i in 1:ncol(all_comb)) {

    i<-all_comb[1,comb_i]
    j<-all_comb[2,comb_i]
    
    jacc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/sum(grepl(i,pippo) | grepl(j,pippo))
    oc_ij<-sum(grepl(i,pippo) & grepl(j,pippo))/min(c(sum(grepl(i,pippo)),sum(grepl(j,pippo))))

    JI_matrix[i,j]<-jacc_ij
    OC_matrix[i,j]<-oc_ij

    }
  
  assign(paste("JI_matrix",method,sep="_"), JI_matrix)
  assign(paste("OC_matrix",method,sep="_"), OC_matrix)
 
} #end for methods 


replicates<-replicates_ordered

### calculate inter-dataset JI

for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  inter_JI_matrix<-JI_matrix[paste(replicates1_ordered,",",sep=""),paste(replicates2_ordered,",",sep="")]
  if(method=="fithic") inter_JI_matrix<-inter_JI_matrix[,-which(colnames(inter_JI_matrix)%in% "rC_merge,")]
  if(method=="hiccups") inter_JI_matrix<-inter_JI_matrix[-which(rownames(inter_JI_matrix)%in% "rA3_l1,"),-which(colnames(inter_JI_matrix)%in% "rA_merge,")]
  
  all_pairwise_JI<-inter_JI_matrix[!is.na(inter_JI_matrix)]
  inter_JI_average<-mean(all_pairwise_JI)
  inter_JI_median<-median(all_pairwise_JI)  

  final_result<-paste(method,inter_JI_average,inter_JI_median,collapse="\t")
  write.table(final_result, paste("Inter-dataset",cell_line,dataset1,"vs",dataset2,"concordance_peaks.txt",sep="_"), sep="\t", quote=F, row.names=F, col.names=F, append=T)
  
  write.table(cbind(method,all_pairwise_JI), paste("Inter-dataset",cell_line,dataset1,"vs",dataset2,"concordance_peaks_all_pairwise_JI.txt",sep="_"),sep="\t",col.names=F, row.names=F, quote=F, append=T)
 
}

################################################################################
### Supplementary Figure 6A 
### Inter-dataset JI heatmaps
################################################################################

JI_max_value<-max(c(max(JI_matrix_hiccups,na.rm=T),max(JI_matrix_gothic,na.rm=T),max(JI_matrix_diffhic,na.rm=T),max(JI_matrix_hippie,na.rm=T),max(JI_matrix_homer,na.rm=T),max(JI_matrix_fithic,na.rm=T)))
replicates<-gsub(",","",replicates)

pdf("SuppFigure6A.pdf")

for (method in methods) {
  JI_matrix<-get(paste("JI_matrix",method,sep="_"))
  plot_heatmap_with_legend(JI_matrix, "Jaccard Index", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Reds", n=9))(100),max_value=JI_max_value, own_scale=F,  type="inter")
}
for (method in methods) {
  OC_matrix<-get(paste("OC_matrix",method,sep="_"))
  plot_heatmap_with_legend(OC_matrix, "Overlap Coefficient", axes.labels=replicates, main.title=method, heatmap.colors=colorRampPalette(brewer.pal(name="Blues", n=9))(100),max_value=1, own_scale=F,  type="inter")
}
dev.off()

################################################################################
### Supplementary Figure 6B 
### inter-dataset JI boxplot
################################################################################

cols<-c("#a50026","#F46D43","#FDAE61","#FEE090","#74ADD1","#4575B4")

interazioni<-read.table("Inter-dataset_IMR90_Rao_vs_Jin_concordance_peaks_all_pairwise_JI.txt",sep="\t",stringsAsFactors=F, header=F,fill=T)
colnames(interazioni)<-c("Method","value")
interazioni$Method<-factor(interazioni$Method, levels=c("hiccups","gothic","homer","diffhic","hippie","fithic"))
mxy <- interazioni

pdf("SuppFigure6B.pdf",width=10)
boxplot(mxy$value ~ mxy$Method, col=cols, main="IMR90 Jin vs Rao",ylab="JI")
dev.off()



