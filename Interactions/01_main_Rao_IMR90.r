
base_dir<-"./Hi-C_dataset"
cell_line<-"IMR90"
dataset<-"Rao" 
samplename<-"Hs_InSitu_MboI"
bin_size<-5000

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

### convert to simple format

### homer

replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")

for (rep in replicates) {

  homer_rep<-file.path(base_dir,dataset,"homer",paste(cell_line,samplename,rep,"homer.txt",sep="_"))
  format_homer(homer_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"homer_formatted.txt",sep="_"))

}

### fithic
replicates<-c("rA5_l1", "rB1_l1", "rB2_l1")
for (rep in replicates) {

  fithic_rep<-file.path(base_dir,dataset,"fithic",paste(cell_line,samplename,rep,"fithic.txt",sep="_"))
  format_fithic(fithic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"fithic_formatted.txt",sep="_"))

}

### gothic
replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
for (rep in replicates) {
              
  gothic_rep<-file.path(base_dir,dataset,"gothic",paste(cell_line,samplename,rep,"gothic.txt",sep="_"))
  format_gothic(gothic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"gothic_formatted.txt",sep="_"))

}

### diffhic
replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
for (rep in replicates) {
              
  diffhic_rep<-file.path(base_dir,dataset,"diffhic",paste(cell_line,samplename,rep,"diffhic.txt",sep="_"))
  format_diffhic_as_bin(diffhic_rep, paste(cell_line,samplename,rep,sep="_"), bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"diffhic_formatted.txt",sep="_"))

}

### hippie
replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
for (rep in replicates) {
              
  hippie_rep<-file.path(base_dir,dataset,"hippie",paste(cell_line,samplename,rep,"hippie.txt"),sep="_")
  format_hippie_as_bin(hippie_rep, paste(cell_line,samplename,rep,sep="_"),bin_size)
  summary_from_file(paste(cell_line,samplename,rep,"hippie_formatted.txt",sep="_"))

}

### hiccups
replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
for (rep in replicates) {
            
  hiccups_rep<-file.path("./compare_peaks/HiCCUPS",dataset,cell_line,paste("MboI",gsub("r","",rep),sep="_"),"hiccups",paste("postprocessed_pixels",as.integer(bin_size),sep="_"))
  format_hiccups(hiccups_rep, paste(cell_line,samplename,rep,sep="_"))
  summary_from_file(paste(cell_line,samplename,rep,"hiccups_formatted.txt",sep="_"))

}


################################################################################
###  compare replicates
base_dir<-"./Hi-C_dataset"
cell_line<-"IMR90"
dataset<-"Rao" 
samplename<-"Hs_InSitu_MboI"
bin_size<-5000


setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")


replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
methods<-c("homer","gothic","diffhic", "hippie", "fithic")
for (method in methods) compare_interactions_with_string_approach(cell_line, samplename, replicates, bin_size, method, write_universe=T, quit_after_writing=F)

replicates<-c("rA1_l1","rA2_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
compare_interactions_with_string_approach(cell_line, samplename, replicates, bin_size, "hiccups", write_universe=T, quit_after_writing=F)

### Jaccard Index

replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
methods<-c("homer","gothic","diffhic", "hippie", "fithic")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="cis", threshold=2)
methods<-c("homer","gothic","diffhic", "hippie")
for (method in methods) calculate_concordance(dataset, cell_line, samplename, replicates, method, type="trans", threshold=2)

replicates<-c("rA1_l1","rA2_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")
calculate_concordance(dataset, cell_line, samplename, replicates, "hiccups", type="cis", threshold=2)

################################################################################
### compare common interactions with chromatin states

dataset<-"Rao" 
cell_line<-"IMR90"
samplename<-"Hs_InSitu_MboI"

setwd(file.path("./compare_peaks",dataset,cell_line))
source("./compare_peaks/00_utils_interactions.r")

methods<-c("homer","gothic","diffhic","hippie")
for (method in methods) {
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="cis")
  compare_with_chromatin_states(dataset, cell_line, samplename, method, interactions_type="trans")
}
compare_with_chromatin_states(dataset, cell_line, samplename, "fithic", interactions_type="cis")
compare_with_chromatin_states(dataset, cell_line, samplename, "hiccups", interactions_type="cis")

################################################################################
### compare with TP
source("./compare_peaks/00_utils_interactions.r")

working_dir<-"./compare_peaks"
setwd(working_dir)

dataset<-"Rao" 
cell_line<-"IMR90"
samplename<-"Hs_InSitu_MboI"
bin_size<-5000

methods<-c("homer","gothic","diffhic", "hippie", "fithic")
replicates<-c("rA1_l1","rA2_l1","rA3_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")

#methods<-c("hiccups")
#replicates<-c("rA1_l1","rA2_l1","rA4_l1","rA5_l1","rB1_l1","rB2_l1")


TP_list<-file.path("./compare_peaks/TP", paste("IMR90_3C_hg19_significant_formatted_",bin_size,".txt",sep=""))
TP_name<-"Jin_3C"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Ferraiuolo_5C_allCellLines_TP_formatted_",bin_size,".txt",sep=""))
TP_name<-"Ferraiuolo_5C_TP"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}

TP_list<-file.path("./compare_peaks/TP", paste("Kim_3C_nonErythroid_TN_formatted_",bin_size,".txt",sep=""))
TP_name<-"Kim_3C_TN"
for (method in methods) {
  hic_common_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(dataset,cell_line,samplename, method,"cis_common.txt",sep="_"))
  compare_interactions_with_TP(TP_list, TP_name, hic_common_interactions_filename, cell_line, bin_size, method, "common", print_found_TP=F)
  for (rep in replicates) {
    hic_rep_interactions_filename<-file.path(working_dir, dataset, cell_line, paste(cell_line,samplename, rep, method,"formatted.txt",sep="_"))
    compare_interactions_with_TP(TP_list, TP_name, hic_rep_interactions_filename, cell_line, bin_size, method, rep, print_found_TP=F)
  }
}



