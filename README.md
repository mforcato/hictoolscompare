# README #


### HiCToolsCompare ###

* Library to compare TADs and interactions identified from Hi-C data with different computational tools
* Examples of how to launch the tools, functions to analyze results, calculate general statistics, and performance metrics used in Forcato et al., "Comparison of computational methods for Hi-C data analysis", Nature Methods, 2017 [in press]
* Hi-C simulated data with simulated interactions, TADs and nested TADs
* Version 1.1
***
### Compared computational methods ###

TAD callers:

* Arrowhead (Durand et al., Cell Systems, 2016)
* domainCaller (Dixon et al., Nature, 2012)
* TADtree (Weinreb et al., Bioinformatics, 2015)
* TADbit (Serra et al., bioRxiv, 2016)
* Insulation Score (Crane et al., Nature, 2015)
* HiCseg (Levy-Leduc et al., Bioinformatics, 2014)
* Armatus (Filippova et al., Algorithms Mol Biol., 2014)

Interaction callers:

* HiCCUPS (Durand et al., Cell Systems, 2016)
* Fit-Hi-C (Ay et al., Genome Res, 2014)
* diffHic (Lun et al., BMC Bioinformatics, 2015)
* GOTHiC (Mifsud et al., Nat Genet, 2015)
* HOMER (Heinz et al., Mol. Cell, 2010)
* HIPPIE (Hwang et al., Bioinformatics, 2015)

### Analyzed Data sets ###

* Lieberman-Aiden (Lieberman-Aiden et al., Science, 2009)
* Sexton (Sexton et al., Cell, 2012)
* Dixon 2012 (Dixon et al., Nature, 2012)
* Jin (Jin et al., Nature, 2012)
* Rao (Rao et al., Cell, 2014)
* Dixon 2015 (Dixon et al., Nature, 2015)

### Available Code and Data ###

Scripts for launching Interaction and TAD callers:

* Scripts_for_general_alignment_and_preprocessing.txt
* Scripts_for_TAD_callers.txt
* Scripts_for_Interactions_callers.txt

Scripts to analyze TAD callers results:

* folder TADs

Scripts to analyze Interaction callers results:

* folder Interactions

Hi-C simulated data and scripts to analyze TAD and interaction callers results on simulated data:

* folder Simulations
***
### How it works ###

* 00_utils_interactions.r and 00_utils_tads.r contain all the R functions to process interaction/TAD callers results
* These function are used in scripts 01_main, there is a script for each dataset
* Scripts from 02_ to 06_ contain code used to generate figures in Forcato et al., Nature Methods, 2017

First, interaction/TAD callers results are preprocessed and converted to a standard format:

TAD
> chr start end

Interactions
> InteractionID bin1_ID bin1_chr bin1_start bin1_end bin2_ID bin2_chr bin2_start bin2_end

The following measures for TADs are calculated:

+ Number and size of TADs
+ Jaccard Indexes and Overlap coefficients for reproducibility of replicates
+ enrichments in Biological Tracks (CTCF)

The following measures for Interactions are calculated:

+ Number of interactions and distance between interacting bins
+ Jaccard Indexes and Overlap coefficients for reproducibility of replicates
+ Classification of interactions based on chromatin states
+ Comparison of interactions with lists of true positive and true negative interaction evidences

### Who do I talk to? ###

* mattia.forcato at unimore.it